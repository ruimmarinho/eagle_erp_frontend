import { Routes } from '@angular/router';

import {ManagementComponent} from "./management/management.component";
import {ManagementFormComponent} from "./management-form/management-form.component";

export const ManagementRoutes: Routes = [
  // { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  {
    path: 'workers',
    component: ManagementComponent,
    data: {
      title: 'Management',
      table: 'workers',
      urls: [
        { title: 'Management', url: '/management' },
        { title: 'Workers' }
      ]
    }
  },
  {
    path: 'workers/add/:id',
    component: ManagementFormComponent,
    data: {
      title: 'Add Worker',
      table: 'workers',
      urls: [
        { title: 'Management', url: '/management' },
        { title: 'Workers', url: '/management/workers' },
        { title: 'Add Worker' }
      ]
    }
  },


  {
    path: 'tasks',
    component: ManagementComponent,
    data: {
      title: 'Management',
      table: 'tasks',
      urls: [
        { title: 'Management', url: '/management' },
        { title: 'Tasks' }
      ]
    }
  },
  {
    path: 'tasks/add/:id',
    component: ManagementFormComponent,
    data: {
      title: 'Add Task',
      table: 'tasks',
      urls: [
        { title: 'Management', url: '/management' },
        { title: 'Tasks', url: '/management/tasks' },
        { title: 'Add Task' }
      ]
    }
  },


  {
    path: 'products',
    component: ManagementComponent,
    data: {
      title: 'Management',
      table: 'products',
      urls: [
        { title: 'Management', url: '/management' },
        { title: 'Products' }
      ]
    }
  },
  {
    path: 'products/add/:id',
    component: ManagementFormComponent,
    data: {
      title: 'Add Products',
      table: 'products',
      urls: [
        { title: 'Management', url: '/management' },
        { title: 'Products', url: '/management/products' },
        { title: 'Add Products' }
      ]
    }
  },


  {
    path: 'machines',
    component: ManagementComponent,
    data: {
      title: 'Management',
      table: 'machines',
      urls: [
        { title: 'Management', url: '/management' },
        { title: 'Machines' }
      ]
    }
  },
  {
    path: 'machines/add/:id',
    component: ManagementFormComponent,
    data: {
      title: 'Add Machines',
      table: 'machines',
      urls: [
        { title: 'Management', url: '/management' },
        { title: 'Machines', url: '/management/machines' },
        { title: 'Add Machine' }
      ]
    }
  }
];
