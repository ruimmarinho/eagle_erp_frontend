import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagementComponent } from './management/management.component';
import {RouterModule} from "@angular/router";
import {ManagementRoutes} from "./management.routing";
import {DatatableComponent} from "./management-components/data-table/data-table.component";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import { ManagementFormComponent } from './management-form/management-form.component';
import {ReactiveFormsModule} from "@angular/forms";
import {PlannerModule} from "../planner/planner.module";



@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ManagementRoutes),
        NgxDatatableModule,
        FontAwesomeModule,
        ReactiveFormsModule,
        PlannerModule,
    ],
  declarations: [
    ManagementComponent,
    DatatableComponent,
    ManagementFormComponent
  ]
})
export class ManagementModule { }
