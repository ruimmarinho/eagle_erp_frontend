import {Component, Input, OnInit, ViewChild} from '@angular/core';

import { faEdit, faTrashCan, faAdd } from '@fortawesome/free-solid-svg-icons';
import {RestApiService} from "../../../../core/http/rest-api.service";
import {Router} from "@angular/router";

declare var require: any;
const data: any = require('./company.json');
@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.css']
})

export class DatatableComponent implements OnInit {

  @Input('tableName') tableName = '';

  editing: any = {};
  rows: any = [];
  temp = [...data];

  loadingIndicator = true;
  reorderable = true;

  editIcon = faEdit;
  trashIcon = faTrashCan;
  addIcon = faAdd;

  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent | undefined;
  constructor(private rest: RestApiService, private router: Router) {}

  goTo(url: string, id: string){
    this.router.navigate([url, id]);
  }

  getData() {
    switch (this.tableName){
      case 'workers':
        this.rest.getWorkers().subscribe((data: {}) => {
          this.rows = data;
        })
        break;
      case 'tasks':
        // this.rest.getTasks().subscribe((data: {}) => {
        //   this.rows = data;
        // })
        break;
      case 'products':
        this.rest.getProducts().subscribe((data: {}) => {
          this.rows = data;
        })
        break;
      case 'machines':
        this.rest.getMachines().subscribe((data: {}) => {
          this.rows = data;
        })
        break;
    }
  }

  daleteData(wid: number) {
    console.log(this.tableName)
    switch (this.tableName){
      case 'workers':
        if (window.confirm('Are you sure, you want to delete?')) {
          this.rest.deleteWorker(wid).subscribe(() => this.getData())
        }
        break;
    }
  }

  updateFilter(event: any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table = data;
  }
  updateValue(event: any, cell: any, rowIndex: any) {
    console.log('inline editing rowIndex', rowIndex);
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    this.rows = [...this.rows];
    console.log('UPDATED!', this.rows[rowIndex][cell]);
  }

  ngOnInit() {
    this.getData();
    this.temp = [...data];
    setTimeout(() => {
      this.loadingIndicator = false;
    }, 1500);
  }
}
