import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {RestApiService} from "../../../core/http/rest-api.service";

@Component({
  selector: 'app-management-form',
  templateUrl: './management-form.component.html',
  styleUrls: ['./management-form.component.css']
})
export class ManagementFormComponent {

  formImport: FormGroup;

  table = '';

  constructor(private router: Router, private rest: RestApiService, private activatedroute: ActivatedRoute) {
    this.formImport = new FormGroup({
      name: new FormControl(),
    });
  }

  save(){
    console.log(this.table)
    const toAdd = JSON.stringify(this.formImport.value);
    switch (this.table){
      case 'workers':
        this.rest.createWorker(toAdd).subscribe(() => this.router.navigateByUrl('management/workers'))
        break;
      case 'tasks':
        this.rest.createTask(toAdd).subscribe(() => this.router.navigateByUrl('management/tasks'))
        break;
      case 'products':
        this.rest.createProduct(toAdd).subscribe(() => this.router.navigateByUrl('management/products'))
        break;
      case 'machines':
        this.rest.createMachine(toAdd).subscribe(() => this.router.navigateByUrl('management/machines'))
        break;
    }
  }

  ngOnInit(): void {
    this.activatedroute.data.subscribe(data => {
      this.table = data['table'];
    })
  }

}
