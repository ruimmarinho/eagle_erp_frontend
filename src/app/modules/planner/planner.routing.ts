import { Routes } from '@angular/router';
import {PlannerPageComponent} from "./planner-page/planner-page.component";
import {Dashboard1Component} from "../dashboards/dashboard1/dashboard1.component";

export const PlannerRoutes: Routes = [
  {
    path: '',
    component: PlannerPageComponent,
    data: {
      title: 'Planner',
      urls: [
        { title: 'Planner', url: '/planner' },
      ]
    }
  }
];
