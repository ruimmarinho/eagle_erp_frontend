import {Component, Input, OnChanges, OnInit} from '@angular/core';
import * as moment from "moment";
import {FunctionsService} from "../../../../core/services/functions.service";
import {Machine} from "../../../../shared/models/machine";

interface CalendarDay {
  day: string;
  dayName: string;
  className: string;
  date: string;
  isWeekend: boolean;
}

interface MachineDays {
  id: number
  name: string;
  days: DayTask[];
}
interface DayTask {
  day: string;
  dayName: string;
  date: string;
  tasks: MachineTask[];
}
interface MachineTask {
  task: string;
  type: string;
  date: string;
  start: string;
  end: string;
  duration: number;
  color: string
}

@Component({
  selector: 'app-timeline-week-day',
  templateUrl: './timeline-week-day.component.html',
  styleUrls: ['./timeline-week-day.component.css']
})
export class TimelineWeekDayComponent implements OnInit, OnChanges {
  @Input() date: any;
  @Input() machinesR!: Machine[];
  @Input() workers: any;
  @Input() request: any;
  hours: string[] = []
  days: CalendarDay[] = [];
  machines: MachineDays[] = []

  constructor(private functions: FunctionsService) { }

  ngOnInit(): void { }

  ngOnChanges() {
    this.getOT()
    // console.log(this.date)
    // console.log(this.request)
    this.hours = this.getHours();
    this.setCalendar();
  }

  getWeekDaysByWeekNumber(weeknumber: any)
  {
    let date = moment().isoWeek(weeknumber||1).startOf("isoWeek"), weeklength=7, result=[];
    while(weeklength--)
    {
      if(date.month() !== moment.utc(this.date, "DD-MM-YYYY").month()){
        result.push(this.createCalendarItem(date, 'fc-other-month'));
      }
      else{
        result.push(this.createCalendarItem(date, 'in-month'));
      }
      date.add(1,"day")
    }
    return result;
  }

  createCalendarItem(data: moment.Moment, className: string) {
    const dayName = data.format('ddd');
    const stringDate = data.format('DD-MM-YYYY');
    return {
      day: data.format('DD'),
      dayName,
      className,
      date: stringDate,
      isWeekend: dayName === 'Sun' || dayName === 'Sat'
    };
  }

  getHours(){
    const items: string[] = [];
    for(let i = 0; i < 24; i++){
      items.push(moment( {hour: i} ).format('HH'));
    }
    return items;
  }

  checkToday(date: string){
    const today = moment().format('DD-MM-YYYY')
    return date === today
  }

  getMachineTasks(){
    const machines: MachineDays[] = []
    let machineDays = {} as DayTask
    this.machinesR.forEach( machine => {
      machines.push(
        {
          id: machine.id,
          name: machine.name,
          days: []
        })
      const tasks = this.request.filter( (task: any) => {
        return task.slots.some( (slot: any) => {
          return slot.machine === machine.id
        });
      });
      const tasksDay = this.functions.splitSlots(tasks)
      console.log(tasksDay)
      this.days.forEach( day => {
        machineDays = {
          day: day.day,
          dayName: day.dayName,
          date: day.date,
          tasks: []
        }
        let empty = 0;
        for(let h = 0; h <= this.hours.length; h++){
          const hour = this.hours[h];

          if (h !== 24){
            const task = tasksDay.filter( (f:any) =>
              moment.utc(f.start, "DD-MM-YYYY").format("DD-MM-YYYY") === day.date &&
              moment.utc(f.start, "DD-MM-YYYY HH:mm").format("HH") === hour
            )[0];

            // console.log(h)
            // console.log(task)

            if(task){
              let addTime = 0;
              if(empty !== 0){
                machineDays.tasks.push(
                  {
                    task: "",
                    type: "",
                    date: "",
                    start: "",
                    end: "",
                    duration: empty,
                    color: ""
                  }
                )
                empty = 0;
              }

              machineDays.tasks.push(
                {
                  task: task.product_reference,
                  type: task.type,
                  date: moment.utc(task.start).format("DD-MM-YYYY"),
                  start: moment.utc(task.start).format("HH:mm"),
                  end: moment.utc(task.end).format("HH:mm"),
                  duration: task.duration,
                  color: task.color
                }
              )
              if(task.duration <= 0){
                addTime += task.duration
                h += addTime;
              }
              else {
                addTime += (task.duration - 1)
                h += addTime;
              }

            }
            else {
              empty++;
            }
          }
          else{
            if(empty !== 0){
              machineDays.tasks.push(
                {
                  task: "",
                  type: "",
                  date: "",
                  start: "",
                  end: "",
                  duration: empty,
                  color: ""
                }
              )
            }
          }
        }
        // console.log(machineDays)
        machines[machines.length-1].days.push(machineDays)
      })
    })
    console.log(machines)
    return machines
  }

  setCalendar(){
    const week = moment(this.date, "DD-MM-YYYY").isoWeek();
    // console.log(week)
    // this.days = this.getWeekDaysByWeekNumber(13);
    this.days = this.getWeekDaysByWeekNumber(week);
    this.machines = this.getMachineTasks();
  }

  getMomentHours(start: moment.Moment, end: moment.Moment){
    return Math.round(moment.duration(end.diff(start)).asHours());
  }

  getOT(){
    this.request.forEach( (task: any) => {
      const color = this.setColorOT()
      task.slots.forEach( (slot: any) => {
        slot.color = color
      })
    })
    console.log(this.request)
  }

  setColorOT(){
    let color = "#";
    for (let i = 0; i < 3; i++){
      color += ("0" + Math.floor(((1 + Math.random()) * Math.pow(16, 2)) / 2).toString(16)).slice(-2);
    }
    return color;
  }

}
