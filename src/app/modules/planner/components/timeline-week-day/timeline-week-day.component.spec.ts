import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelineWeekDayComponent } from './timeline-week-day.component';

describe('TimelineWeekDayComponent', () => {
  let component: TimelineWeekDayComponent;
  let fixture: ComponentFixture<TimelineWeekDayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TimelineWeekDayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineWeekDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
