import {Component, Input, OnInit} from '@angular/core';
import * as moment from "moment";
import { Machine } from 'src/app/shared/models/machine';
import {FunctionsService} from "../../../../core/services/functions.service";


interface CalendarDay {
  day: string;
  dayName: string;
  className: string;
  date: string;
  isWeekend: boolean;
}

interface MachineTask {
  id: number;
  name: string;
  average_load: number;
  tasks: CalendarTask[];
}
interface CalendarTask {
  date: string;
  hours: number;
  week: number;
  product_reference: any;
}


@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {

  @Input() date: any;
  @Input() machines!: Machine[];
  @Input() workers: any;
  @Input() request: any;
  calendar: CalendarDay[] = [];
  days: CalendarDay[] = [];
  tasks: MachineTask[] = []
  weekdaysShort: string[]= []


  constructor(private functions: FunctionsService) { }

  ngOnInit(): void {
    this.setCalendar()
  }

  createCalendar(month: moment.Moment) {
    const daysInMonth = month.daysInMonth();
    const weekDays = moment.weekdaysShort(true);
    this.weekdaysShort = [...weekDays.slice(1), weekDays[0]];
    const calendar: CalendarDay[] = [];

    const clone = month.startOf('months').clone();

    for (let i = 0; i < daysInMonth; i++) {
      calendar.push(this.createCalendarItem(clone, 'in-month'));
      clone.add(1, 'days');
    }

    return calendar;
  }

  createCalendarItem(data: moment.Moment, className: string) {
    const dayName = data.format('ddd');
    const stringDate = data.format('DD-MM-YYYY');
    return {
      day: data.format('DD'),
      dayName,
      className,
      date: stringDate,
      isWeekend: dayName === 'Sun' || dayName === 'Sat'
    };
  }

  checkToday(date: string){
    const today = moment().format('DD-MM-YYYY')
    return date === today
  }

  getTask(){
    const machines: MachineTask[] = []
    this.machines.forEach( machine => {
      const tasks = this.request.filter( (task: any) => {
        return task.slots.some( (slot: any) => {
          return slot.machine === machine.id
        });
      });
      machines.push(
        {
          id: machine.id,
          name: machine.name,
          average_load: 16,
          tasks: []
        }
      )
      const tasksMonth = this.functions.splitSlots(tasks)
      this.days.forEach( day => {
        const taskDay = tasksMonth.filter( (task:any) => {
          return moment.utc(task.start, "DD-MM-YYYY").format("DD-MM-YYYY") === day.date
        });
        let sum = 0;
        const refs: any = []
        if(taskDay.length > 0){
          taskDay.forEach( (task:any) => {
            sum = sum + task.duration
            refs.push(task.product_reference)
          })
          machines[machines.length-1].tasks.push({
            date: day.date,
            hours: sum,
            week: moment(day.date, "DD-MM-YYYY").isoWeek(),
            product_reference: refs
          })
        }
        else {
          machines[machines.length-1].tasks.push({
            date: day.date,
            hours: sum,
            week: moment(day.date, "DD-MM-YYYY").isoWeek(),
            product_reference: refs
          })
        }
      })
    })
    return machines
  }

  displayLoadColor(hours: number, average_load: number){
    const oneQuarter = average_load/4;
    const twoQuarter = average_load/2;
    const threeQuarter = average_load-oneQuarter;
    if(hours <= oneQuarter){
      return 'load-low'
    }
    else if(hours <= threeQuarter){
      return 'load-mid'
    }
    else {
      return 'load-up'
    }
  }

  tipPosition(max: number, i: number){
    if(i < 5){
      return 'on-left'
    }
    else if(i >= (max-5)){
      return 'on-right'
    }
    else{
      return 'on-center'
    }
  }

  seeDay(day: string){
    const weekNumber = moment(day, "DD-MM-YYYY").format("W");
    console.log(weekNumber)
  }

  setCalendar(){
    this.days = this.createCalendar(moment.utc(this.date, "DD-MM-YYYY"));
    // console.log(this.days)
    this.tasks = this.getTask();
    this.calendar = this.days
    // console.log(this.calendar)
    console.log(this.tasks)
  }

}
