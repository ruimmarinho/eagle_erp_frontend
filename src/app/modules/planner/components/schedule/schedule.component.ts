import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import * as moment from "moment";
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import {TimelineComponent} from "../timeline/timeline.component";
import {TimelineWeekDayComponent} from "../timeline-week-day/timeline-week-day.component";
import {Machine} from "../../../../shared/models/machine";
import {GlobalDataService} from "../../../../core/services/global-data.service";
import {RestApiService} from "../../../../core/http/rest-api.service";

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  date = moment.utc();
  today = moment().utc();
  @ViewChild("filters") private filterType: ElementRef<HTMLElement> | any;
  @ViewChild(TimelineComponent) monthView: TimelineComponent | undefined;
  @ViewChild(TimelineWeekDayComponent) weekView: TimelineWeekDayComponent | undefined;
  selected: any = {}

  view = 0;

  faCheck = faCheck;

  allMachines = true
  filterMachines: any = []

  machines: Machine[] = []

  workers: any = []

  request: any = []

  constructor(private global: GlobalDataService, private rest: RestApiService) { }

  ngOnInit(): void {
    this.global.getMachines();
    this.machines = this.global.machines;
    this.setMachineFilters();
    this.rest.getTaskSchedules(this.date.startOf('month').format('YYYY-MM-DD'), this.date.endOf('month').format('YYYY-MM-DD')).subscribe( value => {
      this.request = value;
      console.log(value)
    })
    this.rest.getWorkers().subscribe( value => {
      this.workers = value
      // console.log(value)
    })
    this.global.machineObservable.subscribe(value => {
      this.machines = value;
      this.setMachineFilters()
    })
  }

  setMachineFilters(){
    this.filterMachines = [];
    this.machines.forEach( machine => {
      this.filterMachines.push({
        name: machine.name,
        filter: true
      })
    })
    console.log(this.date)
    setTimeout(() => {
      this.getChildren(1)
    }, 100);
  }

  getChildren(n: number) {
    const parentElement = this.filterType.nativeElement;
    const firstChild = parentElement.children[n];
    this.selected = {
      "type": n,
      "width": firstChild.offsetWidth,
      "left": firstChild.offsetLeft
    }
  }

  selectAllMachines(selected: boolean){
    this.allMachines = !selected
    this.filterMachines.forEach( (machine: any) => {
      machine.filter = this.allMachines;
    })
  }
  selectMachine(m: number){
    this.filterMachines[m].filter = !this.filterMachines[m].filter
    this.allMachines = (this.filterMachines.filter( (machine:any) => machine.filter)).length === this.machines.length
  }

  changeView(view: number){
    console.log(this.date)
    this.view = view;
  }

  public nextmonth() {
    if(this.view === 0){
      this.date.add(1, 'months');
      this.monthView?.setCalendar();
    }
    else{
      this.date.add(1, 'week');
      this.weekView?.setCalendar();
    }
  }

  public previousmonth() {
    if(this.view === 0){
      this.date.subtract(1, 'months');
      this.monthView?.setCalendar();
    }
    else{
      this.date.subtract(1, 'week');
      this.weekView?.setCalendar();
    }
  }

  public todaymonth() {
    this.date = moment();
    this.monthView?.setCalendar();
    this.weekView?.setCalendar();
  }


  @HostListener('window:resize', ['$event'])
  onWindowResize() {
    this.getChildren(this.selected.type);
  }
}
