import {Component,OnInit,} from '@angular/core';
import * as moment from 'moment';
import {Observable, Subject} from "rxjs";

interface CalendarWeek {
  calendarDays: Array<CalendarDay[]>;
  calendarEvents: any;
}

interface CalendarDay {
  day: string;
  dayName: string;
  className: string;
  date: string;
  isWeekend: boolean;
}

interface CalendarEvent {
  maxEvents: number;
  dayName: string;
  className: string;
  date: string;
  isWeekend: boolean;
}

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  date = moment();
  calendar = {} as CalendarWeek;
  days: Array<CalendarDay[]> = [];
  events: Array<CalendarEvent[]> = [];
  weekdaysShort: string[]= []

  ngOnInit(): void {
    // console.log(this.date.date())
    this.days = this.createCalendar(this.date);
    this.setCalendar(this.days)
    console.log(this.calendar)
  }

  createCalendar(month: moment.Moment) {
    const daysInMonth = month.daysInMonth();
    const startOfMonth = month.startOf('months').format('ddd');
    const endOfMonth = month.endOf('months').format('ddd');
    const weekDays = moment.weekdaysShort(true);
    this.weekdaysShort = [...weekDays.slice(1), weekDays[0]];
    const calendar: CalendarDay[] = [];

    const daysBefore = this.weekdaysShort.indexOf(startOfMonth);
    // console.log(this.weekdaysShort.length)
    // console.log(this.weekdaysShort.indexOf(endOfMonth))
    const daysAfter = this.weekdaysShort.length - 1 - this.weekdaysShort.indexOf(endOfMonth);

    const clone = month.startOf('months').clone();
    if (daysBefore > 0) {
      clone.subtract(daysBefore, 'days');
    }

    for (let i = 0; i < daysBefore; i++) {
      calendar.push(this.createCalendarItem(clone, 'fc-other-month'));
      clone.add(1, 'days');
    }

    for (let i = 0; i < daysInMonth; i++) {
      calendar.push(this.createCalendarItem(clone, 'in-month'));
      clone.add(1, 'days');
    }


    for (let i = 0; i < daysAfter; i++) {
      calendar.push(this.createCalendarItem(clone, 'fc-other-month'));
      clone.add(1, 'days');
    }


    return calendar.reduce((pre: Array<CalendarDay[]>, curr: CalendarDay) => {
      if (pre[pre.length - 1].length < this.weekdaysShort.length) {
        pre[pre.length - 1].push(curr);
      } else {
        pre.push([curr]);
      }
      return pre;
    }, [[]]);
  }

  createCalendarItem(data: moment.Moment, className: string) {
    const dayName = data.format('ddd');
    const stringDate = data.format('YYYY-MM-DD');
    return {
      day: data.format('DD'),
      dayName,
      className,
      date: stringDate,
      isWeekend: dayName === 'Sun' || dayName === 'Sat'
    };
  }

  checkToday(date: string){
    const today = moment().format('YYYY-MM-DD')
    return date === today
  }

  setCalendar(days: Array<CalendarDay[]>){
    this.calendar.calendarDays = days
    this.seeEvents()
  }

  haveEvents(event: any){
    return !(Object.keys(event).length === 0);
  }

  MyArray = function() {
    const eTasks: any = [];
    eTasks.push = function() {
      console.log("PUSHING", arguments);
    }
  };


  eTask: any = [];
  updateArray = new Observable(observer => {
    console.log("mudou")
    observer.next(this.eTask);
  });


  tasks = [
    {
      "id" : 123123123,
      "purchase_order": "PO-010203",
      "product": "P1",
      "task_type": "torneado",
      "task_duration": 20,
      "start_time" : 1649667211000,
      "end_time": 1649722620000,
      "hover_info": [
        "turno 1 (08:00-16:00): Worker 0001",
        "turno 2 (16:00-00:00): Worker 0002",
        "turno 3 (08:00-12:00): Worker 0001"
      ]
    },
    {
      "id" : 123123125,
      "purchase_order": "PO-010205",
      "product": "P1",
      "task_type": "torneado",
      "task_duration": 20,
      "start_time" : 1649895359000,
      "end_time": 1650181138000,
      "hover_info": [
        "turno 1 (08:00-16:00): Worker 0001",
        "turno 2 (16:00-00:00): Worker 0002",
        "turno 3 (08:00-12:00): Worker 0001"
      ]
    },
    {
      "id" : 123123126,
      "purchase_order": "PO-010206",
      "product": "P1",
      "task_type": "torneado",
      "task_duration": 20,
      "start_time" : 1649840011000,
      "end_time": 1649926411000,
      "hover_info": [
        "turno 1 (08:00-16:00): Worker 0001",
        "turno 2 (16:00-00:00): Worker 0002",
        "turno 3 (08:00-12:00): Worker 0001"
      ]
    },
    {
      "id" : 123123124,
      "purchase_order": "PO-010204",
      "product": "P1",
      "task_type": "torneado",
      "task_duration": 20,
      "start_time" : 1650672959000,
      "end_time": 1650845760000,
      "hover_info": [
        "turno 1 (08:00-16:00): Worker 0001",
        "turno 2 (16:00-00:00): Worker 0002",
        "turno 3 (08:00-12:00): Worker 0001"
      ]
    }
  ]


  taskCal: any = []

  seeEvents(){
    this.calendar.calendarEvents = []

    const alltasks: any = []
    this.tasks.forEach( (task: any) => alltasks.push(task));

    const events: any[] = []

    this.calendar.calendarDays.forEach( (week, index)=> {
      // events.push([])
      week.forEach( (day, i) => {
        const dayStamp = parseInt(moment.utc(day.date).format('x'))
        this.tasks.forEach( task => {
          if (this.dayBetween(dayStamp, task)) {
            // events[index].push(
            const Difference_In_Time = new Date(task.end_time).getDate() - new Date(task.start_time).getDate();
            const Difference_In_Days = Math.ceil(Difference_In_Time + 1)
            // console.log(task.purchase_order)
            // console.log(Difference_In_Days)
            events.push(
              {
                taskPO: task.purchase_order,
                taskDate: moment(dayStamp).format('YYYY-MM-DD'),
                taskDays: Difference_In_Days
              }
            )
          }
        })
      })
    })

    console.log(events)

    const allDays: any = []
    this.calendar.calendarDays.forEach( (week, i) => {
      allDays.push([])
      week.forEach( days => {
        allDays[i].push(days.date)
      })
    })

    const rows: any = []
    allDays.forEach( (week: any, i: number) => {
      let max = 1
      week.forEach( (day:any) => {
        let n = 0;
        events.forEach( ev => {
          if(day === ev.taskDate){
            n++
          }
        })
        if(max < n){
          max = n;
        }
      })
      rows.push(max)
    })
    console.log(rows)


    const eventsTask: any = []

    allDays.forEach( (week: any, w: number) => {
      eventsTask.push([])
      let n = 0
      // console.log("Week " + w)
      for(let r = 0; r < rows[w]; r++){
        eventsTask[w].push([])
        week.forEach( (day: any, d: number) => {
            // console.log(day)
          eventsTask[w][r].push({day})
        })
      }
    })

    events.forEach( e => {
      if(!(this.eTask.some( (task: any) => task.taskPO === e.taskPO))){
        this.eTask.push(e)
      }
    })

    console.log(eventsTask)

    eventsTask.forEach( (week: any, w: number) => {
      this.taskCal.push([])
      week.forEach( (row: any, r: number) => {
        this.taskCal[w].push([])
        let nDays = 7
        row.forEach( (day: any, d: number) => {
          // console.log(day.day + ' - ' + nDays)
          if(nDays > 0) {
            console.log("Week " + w + " - " + nDays + " - " + d)
            console.log(day.day)
            if(this.checkTaskSameDay(day.day, w, r)){
              const tasks = this.testeM(day.day, nDays);
              console.log(tasks)
              if (Object.keys(tasks).length !== 0) {
                this.taskCal[w][r].push(tasks)
                nDays = nDays - tasks.taskDays
                console.log(tasks.taskDays)
              } else {
                this.taskCal[w][r].push(tasks)
                nDays--;
              }
            }
            else {
              nDays--;
            }
          }
          if (nDays === 0 && d === 7) {
            nDays = 7;
          }
        })
      })
    })

    console.log(this.taskCal)
    console.log(eventsTask)


    // console.log(this.eTask)
    this.calendar.calendarEvents = this.taskCal

    console.log(this.calendar.calendarEvents)
  }

  testeM(tasks: any, wDay: number){
    let arr: any = {};
    // console.log("")
    // console.log(tasks)
    this.eTask.forEach( (task:any, t: any) => {
      if(tasks === task.taskDate){
        if(task.taskDays <= wDay){
          arr = Object.assign({},
            {
              taskPO: task.taskPO,
              taskDate: task.taskDate,
              taskDays: task.taskDays,
            }
          );
          const data = this.eTask.filter( (tasks:any ) => tasks.taskPO !== task.taskPO);
          this.eTask = data
        }
        else{
          arr = Object.assign({},
            {
              taskPO: task.taskPO,
              taskDate: task.taskDate,
              taskDays: wDay,
            }
          )
          const otherDay = moment(task.taskDate).add(wDay, 'days').format("YYYY-MM-DD")
          const arrCopy = Object.assign({},
            {
              taskPO: task.taskPO,
              taskDate: otherDay,
              taskDays: task.taskDays - wDay,
            }
          )
          const data = this.eTask.filter( (tasks: any) => tasks.taskPO !== task.taskPO);
          this.eTask = data
          this.eTask.push(arrCopy)
        }
      }
    })
    return arr
  }

  checkTaskSameDay(checkDay: any, w: number, r: number){
    let add = true
    for(let d = 0; d < this.taskCal[w][r].length; d++){
      // console.log(checkDay)
      // console.log(this.taskCal[w][r][d].taskDate)
      if(Object.keys(this.taskCal[w][r][d]).length !== 0){
        const compareDate = checkDay;
        const startDate   = moment(this.taskCal[w][r][d].taskDate).format("YYYY-MM-DD");
        const toAdd = this.taskCal[w][r][d].taskDays - 1
        const end = moment(this.taskCal[w][r][d].taskDate).add(toAdd, "days").format("YYYY-MM-DD");
        const endDate     = moment(end, "YYYY-MM-DD").format("YYYY-MM-DD");
        // console.log("compare")
        // console.log(compareDate)
        // console.log(startDate)
        // console.log(endDate)
        if(compareDate === startDate || compareDate === endDate){
          // console.log("no Add")
          add = false;
        }
      }
    }
    return add
  }

  tasksForThisDay(dayStamp: number, tasks: any, nDay: number){
    let haveTask = {haveTask: false, taskPO: null, taskDays: 0, taskClass: ''}
    tasks.forEach( (task: any) => {
      if (this.dayBetween(dayStamp, task)) {
        const Difference_In_Time = new Date(task.end_time).getDate() - new Date(task.start_time).getDate();
        const Difference_In_Days = Difference_In_Time + 1
        // console.log(Difference_In_Time)
        // console.log(Difference_In_Days)
        if (new Date(dayStamp).getDate() !== new Date(task.end_time).getDate()){
          haveTask = {
            haveTask: true,
            taskPO: task.purchase_order,
            taskDays: this.setColsToDay(Difference_In_Days, nDay),
            taskClass: this.setClassToDay(Difference_In_Days, nDay)
          }
        }
        else {
          haveTask = {
            haveTask: true,
            taskPO: task.purchase_order,
            taskDays: this.setColsToDay(1, nDay),
            taskClass: 'fc-not-start'
          }
        }
      }
    })
    return haveTask;
  }

  getAllTasksOrder(dayStamp: number){
    const arr: any = []
    this.tasks.forEach( task => {
      if (this.dayBetween(dayStamp, task)) {
        const Difference_In_Time = new Date(task.end_time).getDate() - new Date(task.start_time).getDate();
        const Difference_In_Days = Difference_In_Time
        arr.push(
          {
            taskPO: task.purchase_order,
            taskDate: moment(dayStamp).format('YYYY-MM-DD'),
            // taskDays: this.setColsToDay(Difference_In_Days, 7),
            // taskClass: this.setClassToDay(Difference_In_Days, 7)
          }
        )
      }
    })
    return arr;
  }

  setColsToDay(days: number, ndays: number){
    return ((days > ndays) ? ndays : days)
  }
  setClassToDay(days: number, ndays: number){
    return ((days > ndays) ? 'fc-not-end' : '')
  }

  dayBetween(dayStamp: number, task: any){
    return new Date(dayStamp).getDate() >= new Date(task.start_time).getDate() &&
      new Date(dayStamp).getDate() <= new Date(task.end_time).getDate() &&
      new Date(task.start_time).getMonth() === this.date.month() && new Date(task.start_time).getFullYear() === this.date.year()
  }

  compareDates(dayStamp: string, task: any){
    const day = parseInt(moment.utc(dayStamp).format('x'))
    const start = parseInt(moment.utc(task.taskDate).format('x'))
    const end = parseInt(moment.utc(task.taskDate).add(task.taskDays, 'days').format('x'))
    return day >= start &&
      day <= end &&
      new Date(task.taskDate).getMonth() === this.date.month() && new Date(task.taskDate).getFullYear() === this.date.year()
  }

  getEvent(e: any){
    console.log(e)
    return true
  }

  public nextmonth() {
    this.date.add(1, 'months');
    this.days = this.createCalendar(this.date);
    this.setCalendar(this.days)
  }

  public previousmonth() {
    this.date.subtract(1, 'months');
    this.days = this.createCalendar(this.date);
    this.setCalendar(this.days)
  }
}
