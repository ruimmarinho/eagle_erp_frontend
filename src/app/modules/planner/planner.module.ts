import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlannerPageComponent } from './planner-page/planner-page.component';
import {RouterModule} from "@angular/router";
import {PlannerRoutes} from "./planner.routing";
import {CalendarComponent} from "./components/calendar-incomplete/calendar.component";

import {FormsModule} from "@angular/forms";
import {NgbModalModule} from "@ng-bootstrap/ng-bootstrap";
import { TimelineComponent } from './components/timeline/timeline.component';
import { ScheduleComponent } from './components/schedule/schedule.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import { TimelineWeekDayComponent } from './components/timeline-week-day/timeline-week-day.component';


@NgModule({
  declarations: [
    PlannerPageComponent,
    CalendarComponent,
    TimelineComponent,
    ScheduleComponent,
    TimelineWeekDayComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbModalModule,
    RouterModule.forChild(PlannerRoutes),
    FontAwesomeModule
  ]
})
export class PlannerModule { }
