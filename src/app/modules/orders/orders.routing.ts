import { Routes } from '@angular/router';
import {OrdersPageComponent} from "./orders-page/orders-page.component";

export const OrdersRoutes: Routes = [
  {
    path: '',
    component: OrdersPageComponent,
    data: {
      title: 'Orders',
      urls: [
        { title: 'Orders', url: '/orders' },
      ]
    }
  }
];
