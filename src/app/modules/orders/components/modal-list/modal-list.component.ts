import {Component, ElementRef, HostListener, Input, OnInit, Renderer2} from '@angular/core';
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import { faCheck, faPenToSquare, faMinus, faTimes } from '@fortawesome/free-solid-svg-icons';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RestApiService} from "../../../../core/http/rest-api.service";
import * as moment from "moment";
import {ModalWarningComponent} from "../../../../shared/components/modal-warning/modal-warning.component";

@Component({
  selector: 'app-modal-list',
  templateUrl: './modal-list.component.html',
  styleUrls: ['./modal-list.component.css']
})
export class ModalListComponent implements OnInit {

  @Input() public order: any;
  @Input() public modal: any;

  faCheck = faCheck;
  faPenToSquare = faPenToSquare;
  faMinus = faMinus;
  faTimes = faTimes;

  formModal: FormGroup;
  edit = false
  modalQO: any

  @HostListener('document:click', ['$event'])
  clickout(event: any) {
    if((!this.ElByClassName.nativeElement.contains(event.target)) && this.edit) {
      console.log("entra")
      this.renderer.addClass(this.modalQO[0], 'modal-static');
      setTimeout(() => {
        this.renderer.removeClass(this.modalQO[0], 'modal-static');
      }, 100);
    }
  }


  constructor(private activeModal: NgbActiveModal,
              private modalService: NgbModal,
              private fb: FormBuilder,
              private rest: RestApiService,
              private ElByClassName: ElementRef,
              private renderer: Renderer2
  ) {
    this.formModal = this.fb.group({ })
    this.modalQO = document.getElementsByClassName("QOModal");
  }

  ngOnInit(): void {
    // console.log(this.order)
    this.qoForm();
    // console.log(this.formModal)
  }

  editQO() {
    this.edit = true;
    this.renderer.addClass(this.modalQO[0], 'editing');
  }

  saveQO(){
    const modalWarning = this.modalService.open(ModalWarningComponent,  { centered: true, windowClass: 'lg' });
    modalWarning.componentInstance.title = "Update Quotation Order";
    modalWarning.componentInstance.text = "Are you sure you want to change the data of the Quotation Order: " + this.order.reference + "?";
    modalWarning.componentInstance.btns = [{text: "Yes", click: "confirm"}, {text: "No", click: "close"}];
    modalWarning.result.then((result) => {
      if(result){
        // console.log("confirm")
        const products: any = []
        for (const key in this.formModal.controls) {
          products.push({
            family: this.formModal.value[key].family,
            subfamily: this.formModal.value[key].subfamily,
            finish_class: this.formModal.value[key].finish_class,
            material: this.formModal.value[key].material,
            diameter: this.formModal.value[key].diameter,
            weight: this.formModal.value[key].weight,
            dar: this.formModal.value[key].dar,
            z: this.formModal.value[key].z,
            surfaces: this.formModal.value[key].surfaces,
            cone_diameter: this.formModal.value[key].cone_diameter,
            templates: this.formModal.value[key].templates,
            porting_hole: this.formModal.value[key].porting_hole,
            additional_parameters: this.formModal.value[key].additional_parameters,
          })
        }
        let toSend = {
          id: this.order.id,
          reference: this.order.reference,
          customer: this.order.customer,
          creation: moment(this.order.creation, "DD-MM-YYYY").format("YYYY-MM-DD"),
          target_date: moment(this.order.target_date, "DD-MM-YYYY").format("YYYY-MM-DD"),
          status: this.order.status,
          products: [] as any
        }

        products.forEach( (product: any) => {
          toSend.products.push(product)
        })

        // console.log(toSend)

        this.rest.updateQuotationOrder(toSend).subscribe(() => {
          this.updatedQO();
        })
        this.edit = false;

      } else {
        // console.log("no confirm")
        this.edit = false;
        this.order.products.forEach( (product: any, p: number) => {
          this.formModal.removeControl("prod"+p)
        })
        this.qoForm();
      }
    }).catch((error) => {
      // console.log(error);
    });
    this.renderer.removeClass(this.modalQO[0], 'editing');
  }
  cancelQO(){
    this.edit = false;
    this.renderer.removeClass(this.modalQO[0], 'editing');
  }

  qoForm(){
    this.order.products.forEach( (product: any, p: number) => {
      this.formModal.addControl("prod"+p, this.fb.group({
        family: [product.family, Validators.required],
        subfamily: [product.subfamily, Validators.required],
        finish_class: [product.finish_class, Validators.required],
        material: [product.material, Validators.required],
        diameter: [product.diameter, Validators.required],
        weight: [product.weight, Validators.required],
        dar: [product.dar, Validators.required],
        z: [product.z, Validators.required],
        surfaces: [product.surfaces, Validators.required],
        cone_diameter: [product.cone_diameter, Validators.required],
        templates: [product.templates, Validators.required],
        porting_hole: [product.porting_hole, Validators.required],
        additional_parameters: [product.additional_parameters, Validators.required]
      }))
    })
  }

  confirmQO() {
    const modalWarning = this.modalService.open(ModalWarningComponent,  { centered: true, windowClass: 'lg' });
    modalWarning.componentInstance.title = "Confirm Quotation Order";
    modalWarning.componentInstance.text = "Are you sure you want to confirm the Quotation Order: " + this.order.reference + "?";
    modalWarning.componentInstance.btns = [{text: "Yes", click: "confirm"}, {text: "No", click: "close"}];
    modalWarning.result.then((result) => {
      if(result){
        // console.log("confirm")
        this.activeModal.close(1);
      } else {
        // console.log("no confirm")
      }
    }).catch((error) => {
      // console.log(error);
    });
  }

  updatedQO() {
    this.activeModal.close(2);
  }

  closeModal() {
    this.activeModal.close('Modal Closed');
  }

}
