import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QoListComponent } from './qo-list.component';

describe('QoListComponent', () => {
  let component: QoListComponent;
  let fixture: ComponentFixture<QoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
