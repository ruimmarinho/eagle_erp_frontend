import { Component, OnInit } from '@angular/core';
import { faCheck, faEye } from '@fortawesome/free-solid-svg-icons';
import {RestApiService} from "../../../../core/http/rest-api.service";
import * as moment from "moment";
import {PurchaseOrderService} from "../../../../core/services/purchase-order.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ModalListComponent} from "../modal-list/modal-list.component";
import {ToastService} from "../../../../core/services/toast.service";

@Component({
  selector: 'app-qo-list',
  templateUrl: './qo-list.component.html',
  styleUrls: ['./qo-list.component.css']
})
export class QoListComponent implements OnInit {

  faCheck =  faCheck;
  faEye = faEye;

  rows: any = [];

  ot = { show: true }

  constructor(
    private rest: RestApiService,
    private purchase_order: PurchaseOrderService,
    private modalService: NgbModal,
    private toastService: ToastService
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  openModal(qo: any){
    const modalRef = this.modalService.open(ModalListComponent,  { centered: true, windowClass: 'xxl-modal QOModal' });
    modalRef.componentInstance.order = qo;
    modalRef.componentInstance.modal = modalRef;
    modalRef.result.then((result) => {
      // console.log(result);
      if(result === 1){
        this.confirmQO(qo.reference)
      } else if (result === 2){
        this.getData();
      }
    }).catch((error) => {
      // console.log(error);
    });
  }

  confirmQO(ref: string){
    console.log(ref)
    this.rest.addPurchaseOrder(ref).subscribe( () => {
      this.rest.getPurchaseOrder().subscribe( (pos: any) => {
        const purchase: any = []
        pos.forEach( (po: any) => {
          const products = po.products
          purchase.push({
            id: po.id,
            reference: po.reference,
            customer: po.customer,
            quotation: po.quotation,
            creation: moment(po.creation).format("DD-MM-YYYY"),
            target_date: moment(po.target_date).format("DD-MM-YYYY"),
            products: products
          })
        })
        this.purchase_order.addPurchase(purchase)
        this.getData();
        this.toastService.addToast({type: "success", text: "Quotation Order successfully added.", duration: 3})
      })
    })
  }

  getData() {
    this.rest.getQuotationOrder().subscribe( (qos: any) => {
      this.rows = [];
      qos.forEach( (qo: any) => {
        const products = qo.products
        this.rows.push({
          id: qo.id,
          reference: qo.reference,
          customer: qo.customer,
          creation: moment(qo.creation).format("DD-MM-YYYY"),
          target_date: moment(qo.target_date).format("DD-MM-YYYY"),
          status: qo.status,
          products: products
        })
      })
      console.log(this.rows)
    })
  }

}
