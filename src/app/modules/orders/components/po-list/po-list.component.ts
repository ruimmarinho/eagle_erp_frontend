import { Component, OnInit } from '@angular/core';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import * as moment from "moment";
import {RestApiService} from "../../../../core/http/rest-api.service";
import {Subscription} from "rxjs";
import {PurchaseOrderService} from "../../../../core/services/purchase-order.service";

@Component({
  selector: 'app-po-list',
  templateUrl: './po-list.component.html',
  styleUrls: ['./po-list.component.css']
})
export class PoListComponent implements OnInit {

  faCheck =  faCheck;

  rows: any = [];

  subscription: Subscription;

  constructor(private rest: RestApiService, private purchase_order: PurchaseOrderService) {
    this.subscription = this.purchase_order.current_purchases.subscribe( (data: any) => this.rows = data)
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.rest.getPurchaseOrder().subscribe( (pos: any) => {
      const purchase: any = []
      pos.forEach( (po: any) => {
        const products = po.products
        purchase.push({
          id: po.id,
          reference: po.reference,
          customer: po.customer,
          quotation: po.quotation,
          creation: moment(po.creation).format("DD-MM-YYYY"),
          target_date: moment(po.target_date).format("DD-MM-YYYY"),
          products: products
        })
      })
      this.purchase_order.addPurchase(purchase)
    })
  }

}
