import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersPageComponent } from './orders-page/orders-page.component';
import {RouterModule} from "@angular/router";
import {OrdersRoutes} from "./orders.routing";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import { QoListComponent } from './components/qo-list/qo-list.component';
import { PoListComponent } from './components/po-list/po-list.component';
import { ModalListComponent } from './components/modal-list/modal-list.component';
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    OrdersPageComponent,
    QoListComponent,
    PoListComponent,
    ModalListComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(OrdersRoutes),
    NgxDatatableModule,
    FontAwesomeModule,
    ReactiveFormsModule,
  ]
})
export class OrdersModule { }
