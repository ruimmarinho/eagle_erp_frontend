import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-eagle-view-page',
  templateUrl: './eagle-view-page.component.html',
  styleUrls: ['./eagle-view-page.component.css']
})
export class EagleViewPageComponent implements OnInit {

  data: any

  constructor() { }

  ngOnInit(): void {
  }

  dataChangedHandler(data: any) {
    if(!(JSON.stringify(this.data)==JSON.stringify(data))){
      this.data = data
    }
    // console.log(data);
  }

}
