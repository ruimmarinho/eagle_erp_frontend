import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EagleViewPageComponent } from './eagle-view-page.component';

describe('EagleViewPageComponent', () => {
  let component: EagleViewPageComponent;
  let fixture: ComponentFixture<EagleViewPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EagleViewPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EagleViewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
