import { Routes } from '@angular/router';
import {EagleViewPageComponent} from "./eagle-view-page/eagle-view-page.component";


export const EagleViewRoutes: Routes = [
  {
    path: '',
    component: EagleViewPageComponent,
    data: {
      title: 'Eagle View',
      urls: [
        { title: 'Eagle View', url: '/eagleview' },
      ]
    }
  }
];
