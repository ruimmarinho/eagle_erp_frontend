import {Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import { faGear, faTriangleExclamation, faCheck, faBan } from '@fortawesome/free-solid-svg-icons';
import * as moment from "moment";

interface Product {
  product: string;
  product_duration: number;
  product_status: string;
  product_weight: number
}
interface PO {
  order: string;
  products: Product[];
  status: string;
  duration: number;
  start: string;
  end: string;
  delay: number;
}

interface State {
  preview: number;
  done: number;
  percentage: number;
}
interface Resume {
  ots: State;
  weight: State;
  hours: State;
  products: State
}

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit, OnChanges {

  @Input() department!: string;
  @Input() info: any;

  purchaseOrders: PO[] = []
  resume = {
    ots: {preview: 0, done: 0, percentage: 0},
    weight: {preview: 0, done: 0, percentage: 0},
    hours: {preview: 0, done: 0, percentage: 0},
    products: {preview: 0, done: 0, percentage: 0}
  } as Resume;

  @ViewChild("card") private card: ElementRef<HTMLElement> | any;
  minHeight = 315;

  faGear = faGear;
  faTriangleExclamation = faTriangleExclamation;
  faCheck = faCheck;
  faBan = faBan;

  percentage = 40

  working = true
  inProgress = false

  myMap = new Map();

  constructor() { }

  ngOnInit(): void {
    // setTimeout(() => {
    //   console.log(this.card.nativeElement.offsetHeight)
    // }, 100);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!changes['info'].firstChange) {
      // console.log(changes['info'].currentValue)
      const products = changes['info'].currentValue
      // console.log(products)

      this.myMap.clear();
      this.purchaseOrders = []
      this.resume = {
        ots: {preview: 0, done: 0, percentage: 0},
        weight: {preview: 0, done: 0, percentage: 0},
        hours: {preview: 0, done: 0, percentage: 0},
        products: {preview: 0, done: 0, percentage: 0}
      } as Resume;

      for(let i = 0 ; i < products.length; i++){
        const product = products[i]
        const full_ref = this.getReference(product.reference, "po") + "-" + this.getReference(product.reference, "product")
        const po = this.getReference(product.reference, "po")
        const prod = this.getReference(product.reference, "product")
        const prod_duration = product.duration
        const prod_status = product.status
        const prod_weight = product.product_weight
        const dep = product.department
        const stage_status = product.stage_status
        const start_time = product.start_time
        const end_time = product.end_time
        // console.log(product)
        // console.log(po)
        // console.log(prod)

        if(this.myMap.has(full_ref)){
          this.myMap.get(full_ref).duration += prod_duration
          this.myMap.get(full_ref).products.push({
            product: prod,
            product_duration: prod_duration,
            product_status: prod_status,
            product_weight: prod_weight
          })
        }
        else {
          this.myMap.set(full_ref, {
            order: po,
            department: dep,
            duration: prod_duration,
            products: [{
                product: prod,
                product_duration: prod_duration,
                product_status: prod_status,
                product_weight: prod_weight
              }],
            status: '',
            start: start_time,
            end: end_time
          });
        }
      }

      // console.log(map1)
      // console.log(this.myMap)

      this.myMap.forEach((value, key) => {
        let delay = 0;
        if(moment.utc().diff(moment.utc(value.end, "YYYY-MM-DD"), "days") > 0){
          delay = moment.utc().diff(moment.utc(value.end, "YYYY-MM-DD"), "days")
        }
        this.purchaseOrders.push({
          order: value.order,
          products: value.products,
          status: value.status,
          duration: value.duration,
          start: moment.utc(value.start).format("DD-MM-YYYY"),
          end: moment.utc(value.end).format("DD-MM-YYYY"),
          delay: delay
        })
      });


      this.resume.ots.preview = this.purchaseOrders.length
      this.purchaseOrders.forEach( po => {
        po.status = this.getStatus(po.products)
        this.resume.products.preview += po.products.length
        this.resume.hours.preview += po.duration
        po.products.forEach( product => {
          this.resume.weight.preview = parseFloat((this.resume.weight.preview + (product.product_weight / 1000)).toFixed(1));
          if(product.product_status === "Finished"){
            this.resume.weight.done += Math.round( (product.product_weight / 1000));
            this.resume.hours.done += product.product_duration
          }
        })
        if(po.status === "Finished"){
          this.resume.ots.done += 1
          this.resume.products.done += 1
        }
      })

      console.log(this.purchaseOrders)
      // console.log(this.resume)

      this.addToResume()

      // if(this.resume.preview !== 0){
      //   this.resume.percentage = Math.round((this.resume.done / this.resume.preview) * 100)
      // }
      // else {
      //   this.resume.percentage = 0
      // }
    }
  }

  getStatus(products: any){
    let status= '';
    const nProducts = products.length;
    const finished = products.filter( (product: any) => product.product_status === "Finished").length;
    const pending = products.filter( (product: any) => product.product_status === "Pending").length;
    const inProgress = products.filter( (product: any) => product.product_status === "InProgress").length;

    if(inProgress !== 0){
      status = "InProgress"
    }
    else {
      if(finished === nProducts) {
        status = "Finished"
      }
      else if(pending === nProducts) {
        status = "Pending"
      }
      else {
        status = "Paused"
      }
    }

    return status
  }

  getReference(ref: string, level: string){
    switch (level) {
      case "po":
        return ref.split("-").slice(0, -3).join("-")
      case "product":
        return ref.split("-")[(ref.split("-").length) - 3];
      case "stage":
        return ref.split("-")[(ref.split("-").length) - 2];
      case "task":
        return ref.split("-")[(ref.split("-").length) - 1];
    }
    return ''
  }

  addToResume(){
    switch (this.department){
      case "ingenieria":
        if(this.resume.ots.preview !== 0){
          this.resume.ots.percentage = Math.round((this.resume.ots.done / this.resume.ots.preview) * 100)
        }
        else {
          this.resume.ots.percentage = 0
        }
        break;
      case "fundicion":
        if(this.resume.weight.preview !== 0){
          this.resume.weight.percentage = Math.round((this.resume.weight.done / this.resume.weight.preview) * 100)
        }
        else {
          this.resume.weight.percentage = 0
        }
        break;
      case "calidad":
        if(this.resume.products.preview !== 0){
          this.resume.products.percentage = Math.round((this.resume.products.done / this.resume.products.preview) * 100)
        }
        else {
          this.resume.products.percentage = 0
        }
        break;
      case "expedicion":
        if(this.resume.ots.preview !== 0){
          this.resume.ots.percentage = Math.round((this.resume.ots.done / this.resume.ots.preview) * 100)
        }
        else {
          this.resume.ots.percentage = 0
        }
        break;
      default:
        if(this.resume.hours.preview !== 0){
          this.resume.hours.percentage = Math.round((this.resume.hours.done / this.resume.hours.preview) * 100)
        }
        else {
          this.resume.hours.percentage = 0
        }
        break;
    }
  }
}
