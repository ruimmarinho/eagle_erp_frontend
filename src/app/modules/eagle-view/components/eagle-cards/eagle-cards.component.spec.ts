import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EagleCardsComponent } from './eagle-cards.component';

describe('EagleCardsComponent', () => {
  let component: EagleCardsComponent;
  let fixture: ComponentFixture<EagleCardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EagleCardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EagleCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
