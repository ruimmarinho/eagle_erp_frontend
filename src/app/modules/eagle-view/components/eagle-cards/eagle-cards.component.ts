import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import SwiperCore, { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-eagle-cards',
  templateUrl: './eagle-cards.component.html',
  styleUrls: ['./eagle-cards.component.css']
})
export class EagleCardsComponent implements OnInit, OnChanges {

  @Input() data: any;

  departments = ["ingenieria", "fundicion", "mecanizado", "pulido", "calidad", "expedicion"]

  products: any = [[],[],[],[],[],[]]

  info: any

  config: SwiperOptions = {
    slidesPerView: 4,
    spaceBetween: 50,
    pagination: { clickable: true }
  };
  onSwiper(swiper: any) {
    // console.log(swiper);
  }
  onSlideChange() {
    // console.log('slide change');
  }

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!changes['data'].firstChange) {
      // console.log(changes['data'].currentValue)
      const POs = changes['data'].currentValue
      this.getTaskByDepartment(POs)
      // console.log(this.products)
    }
    // console.log(this.info)
  }

  getTaskByDepartment(POs: any){
    const prods: any = [[],[],[],[],[],[]]
    for(let po of POs){
      for(let product of po.products){
        for(let stage of product.stages){
          for(let task of stage.tasks){
            for(let i = 0; i < this.departments.length; i++){
              const department = this.departments[i]
              if(task.department === department){
                prods[i].push({
                  stage_status: stage.status,
                  department: task.department,
                  duration: task.duration,
                  reference: task.reference,
                  status: task.status,
                  task_type: task.task_type,
                  product_weight: product.weight,
                  start_time: stage.schedule.start_time,
                  end_time: stage.schedule.end_time
                })
              }
            }
          }
        }
      }
    }
    this.products = prods
  }

}
