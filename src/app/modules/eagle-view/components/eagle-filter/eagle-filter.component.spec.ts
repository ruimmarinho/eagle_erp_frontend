import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EagleFilterComponent } from './eagle-filter.component';

describe('EagleFilterComponent', () => {
  let component: EagleFilterComponent;
  let fixture: ComponentFixture<EagleFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EagleFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EagleFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
