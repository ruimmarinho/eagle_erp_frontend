import {Component, ElementRef, HostListener, OnInit, Output, ViewChild, EventEmitter} from '@angular/core';
import { faCalendarDays, faList } from '@fortawesome/free-solid-svg-icons';
import {RestApiService} from "../../../../core/http/rest-api.service";
import * as moment from "moment";
import {NavigationStart, Router} from "@angular/router";

@Component({
  selector: 'app-eagle-filter',
  templateUrl: './eagle-filter.component.html',
  styleUrls: ['./eagle-filter.component.css']
})
export class EagleFilterComponent implements OnInit {

  @Output() dataChanged: EventEmitter<any> =   new EventEmitter();

  @HostListener('window:resize', ['$event'])
  onWindowResize() {
    this.getChildren(this.selected.type);
  }

  @ViewChild("filters") private filterType: ElementRef<HTMLElement> | any;
  selected: any = {}

  startDate: any
  endDate: any
  start!: string
  end!: string
  faCalendarDays = faCalendarDays
  faList = faList

  dataTimer: any;

  constructor(private rest: RestApiService, private router: Router) { }

  ngOnInit(): void {
    this.dateWeek()
    setTimeout(() => {
      this.getChildren(1)
      this.getData()
    }, 100);


    this.dataTimer = setInterval(() => {
      this.getData()
    }, 6000)

    this.router.events.subscribe((event) => {
      if(event instanceof NavigationStart) {
        if (this.dataTimer) {
          clearInterval(this.dataTimer);
        }
      }
    })
  }

  getData(){
    // console.log(this.start + " - " + this.end)
    this.rest.getPurchaseOrderByDates(this.start, this.end).subscribe( data => {
      // console.log(data)
      this.dataChanged.emit(data)
    })
  }

  getChildren(n: number) {
    const parentElement = this.filterType.nativeElement;
    const firstChild = parentElement.children[n];
    this.selected = {
      "type": n,
      "width": firstChild.offsetWidth,
      "left": firstChild.offsetLeft
    }
    switch (n){
      case 0:
        this.dateWeek()
        break;
      case 1:
        this.date2Weeks()
        break;
      case 2:
        this.dateMonth()
        break;
    }
    this.getData()
    this.startDate = null;
    this.endDate = null;
  }

  dateWeek(){
    const today = moment();
    this.start = today.startOf('isoWeek').isoWeekday(1).format('YYYY-MM-DD');
    this.end = today.endOf('isoWeek').format('YYYY-MM-DD');
  }
  date2Weeks(){
    const today = moment();
    this.start = today.startOf('isoWeek').isoWeekday(1).format('YYYY-MM-DD');
    this.end = today.add(1, 'weeks').endOf('isoWeek').format('YYYY-MM-DD');
  }
  dateMonth(){
    const today = moment();
    this.start = today.startOf('month').format('YYYY-MM-DD');
    this.end = today.endOf('month').format('YYYY-MM-DD');
  }

  setStartDate(){
    this.endDate = null;
  }
  setEndDate(){
    this.start = moment(this.startDate.day + "-" + this.startDate.month + "-" + this.startDate.year, "DD-MM-YYYY").format('YYYY-MM-DD')
    this.end = moment(this.endDate.day + "-" + this.endDate.month + "-" + this.endDate.year, "DD-MM-YYYY").format('YYYY-MM-DD')
    this.rest.getPurchaseOrderByDates(this.start, this.end).subscribe( data => {
      // console.log(data)
      this.selected = {}
      this.dataChanged.emit(data)
    })
  }

}
