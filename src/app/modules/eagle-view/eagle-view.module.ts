import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EagleViewPageComponent } from './eagle-view-page/eagle-view-page.component';
import {RouterModule} from "@angular/router";
import {EagleViewRoutes} from "./eagle-view.routing";
import { EagleFilterComponent } from './components/eagle-filter/eagle-filter.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule} from "@angular/forms";
import {NgbDatepickerModule} from "@ng-bootstrap/ng-bootstrap";
import { EagleCardsComponent } from './components/eagle-cards/eagle-cards.component';
import { CardComponent } from './components/eagle-cards/card/card.component';
import {SwiperModule} from "swiper/angular";


@NgModule({
  declarations: [
    EagleViewPageComponent,
    EagleFilterComponent,
    EagleCardsComponent,
    CardComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(EagleViewRoutes),
    FontAwesomeModule,
    FormsModule,
    NgbDatepickerModule,
    SwiperModule,
  ]
})
export class EagleViewModule { }
