import {Component} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {RestApiService} from "../../../../core/http/rest-api.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import { faEdit, faTrashCan, faAdd, faMinus, faCheck, faTimes } from '@fortawesome/free-solid-svg-icons';
import {ProductModalComponent} from "../product-modal/product-modal.component";
import {ProductionCycleService} from "../../../../core/services/production-cycle.service";
import {QuotationOrderService} from "../../../../core/services/quotation-order.service";
import {ToastService} from "../../../../core/services/toast.service";

@Component({
  selector: 'app-quotation-order',
  templateUrl: './quotation-order.component.html',
  styleUrls: ['./quotation-order.component.css']
})
export class QuotationOrderComponent {

  formImport: FormGroup;

  products: any = []

  table = '';

  editIcon = faEdit;
  trashIcon = faTrashCan;
  addIcon = faAdd;
  removeIcon = faMinus;
  check = faCheck;
  times = faTimes;

  constructor(
    private router: Router,
    private rest: RestApiService,
    private activatedroute: ActivatedRoute,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private production_cycle: ProductionCycleService,
    private quotations: QuotationOrderService,
    private toastService: ToastService
  ) {
    this.formImport = this.fb.group({});
    this.resetFormClient();
  }

  resetFormClient(){
    this.formImport = this.fb.group({
      customer: ['', Validators.required],
      target_date: ['', Validators.required]
    });
  }

  addProduct() {
    this.modalService.open(ProductModalComponent,
      {
        ariaLabelledBy: 'modal-basic-title',
        centered: true
      }).result.then((result) => {
      // this.closeResult = `Closed with: ${result}`;

      console.log(result)

      if(result){
        const i = this.productOnList(result)

        if(i !== -1){
          this.products[i].quantity = this.products[i].quantity + result.quantity;
        } else {
          this.products.push(result)
        }

        console.log(this.products)
      }

    }, (reason) => {
      // console.log(reason)
    });
  }

  productOnList(p: any): number{
    const keys = [
      "family",
      "finish_class",
      "material",
      "diameter",
      "weight",
      "dar",
      "z",
      "surfaces",
      "cone_diameter",
      "plates",
      "hole"]
    const p1 = keys.reduce((result, key) => ({
      ...result,
      [key]: p[key]
    }), {});

    for (let i = 0; i < this.products.length; i++) {
      const p2 = keys.reduce((result, key) => ({
        ...result,
        [key]: this.products[i][key]
      }), {});
      if(JSON.stringify(p1) === JSON.stringify(p2)){
        console.log(p1)
        console.log(p2)
        return i
      }
    }
    return -1
  }

  removeProduct(index: number){
    console.log(index)
    this.products.splice(index, 1);
    console.log(this.products)
  }

  plusProduct(index: number){
    this.products[index].quantity++;
    console.log(this.products)
  }
  minusProduct(index: number){
    this.products[index].quantity > 1 ? this.products[index].quantity-- : this.products[index].quantity;
    console.log(this.products)
  }

  calculate(){
    let formObj = this.formImport.getRawValue();
    formObj["products"] = [];
    console.log(this.products)
    this.products.forEach( (product: any) => {
      for (let i = 0; i < product.quantity; i++) {
        formObj["products"].push({
          family: product.family,
          subfamily: product.subfamily,
          finish_class: product.finish_class,
          material: product.material,
          diameter: product.diameter,
          weight: product.weight,
          cone_diameter: product.cone_diameter,
          dar: product.dar,
          porting_hole: product.porting_hole,
          surfaces: product.surfaces,
          templates: product.templates,
          z: product.z,
          additional_parameters: []
        })
      }
    })
    console.log(formObj)
    console.log(JSON.stringify(formObj))
    // const response = await this.rest.getProductCycles(JSON.stringify(formObj)).toPromise();
    // console.log(response)
    // if(response?.status === 201){
    //     console.log(response.body);
    //     this.production_cycle.changeProducts(response.body)
    //     this.quotations.quotations.push(response.body)
    //     this.toastService.addToast({type: "success", text: "Quotation Order successfully added.", duration: 3})
    // }
    this.rest.getProductCycles(JSON.stringify(formObj)).subscribe((data: any) => {
      if(data.status === 201) {
        console.log(data.body);
        this.production_cycle.changeProducts(data.body)
        this.quotations.quotations.push(data.body)
        this.toastService.addToast({type: "success", text: "Quotation Order successfully added.", duration: 3})
      }
    })
  }

  cleanData(){
    this.resetFormClient();
    this.products = []
    this.production_cycle.changeProducts(null)
  }

  addQO(){
    let formObj = this.formImport.getRawValue();
    formObj["products"] = [];
    console.log(this.products)
    this.products.forEach( (product: any) => {
      for (let i = 0; i < product.quantity; i++) {
        formObj["products"].push({
          family: product.family,
          subfamily: product.subfamily,
          finish_class: product.finish_class,
          material: product.material,
          diameter: product.diameter,
          weight: product.weight,
          cone_diameter: product.cone_diameter,
          dar: product.dar,
          porting_hole: product.porting_hole,
          surfaces: product.surfaces,
          templates: product.templates,
          z: product.z,
          additional_parameters: []
        })
      }
    })
    console.log(formObj)
    console.log(JSON.stringify(formObj))

    this.rest.addQuotationOrder(JSON.stringify(formObj)).subscribe((data: any) => {
      if(data.status === 201) {
        console.log(data.body);
        this.production_cycle.changeProducts(data.body)
        this.quotations.quotations.push(data.body)
        this.toastService.addToast({type: "success", text: "Quotation Order successfully added.", duration: 3})
      }
    })
  }

  productsJSON(prod : any){
    const products = []
    for (let i = 0; i < prod.quantity; i++) {
      console.log(prod)
      products.push(JSON.parse(JSON.stringify(prod)))
    }
    console.log(products)
    return products
  }

  ngOnInit(): void {
    this.activatedroute.data.subscribe(data => {
      this.table = data['table'];
    })
  }

}
