import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProductionCycleService} from "../../../../core/services/production-cycle.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-estimated-time',
  templateUrl: './estimated-time.component.html',
  styleUrls: ['./estimated-time.component.css']
})
export class EstimatedTimeComponent implements OnInit, OnDestroy {

  quotation_products: any = null;
  subscription: Subscription;

  constructor(private production_cycle: ProductionCycleService) {
    this.subscription = this.production_cycle.current_products.subscribe( (message: any) => this.quotation_products = message)
  }

  ngOnInit() { }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
