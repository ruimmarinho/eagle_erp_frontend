import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-product-modal',
  templateUrl: './product-modal.component.html',
  styleUrls: ['./product-modal.component.css']
})
export class ProductModalComponent implements OnInit {

  formModal: FormGroup;

  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder) {
    this.formModal = this.fb.group({});
    this.resetFormProducts()
  }

  showInputs(type: string){
    return this.formModal.controls['family'].value === type;
  }

  resetFormProducts(){
    this.formModal = this.fb.group({
      family: ['', Validators.required],
      subfamily: ['', Validators.required],
      finish_class: ['', Validators.required],
      material: ['', Validators.required],
      diameter: ['', Validators.required],
      weight: ['', Validators.required],
      dar: ['', Validators.required],
      z: ['', Validators.required],
      surfaces: [false, Validators.required],
      cone_diameter: ['', Validators.required],
      templates: [false, Validators.required],
      porting_hole: [false, Validators.required],
      quantity: ['', Validators.required],
      additional_parameters: [[], Validators.required]
    })
  }

  setSubFamily(){
    // console.log(this.formModal.value.diameter)
    const diameter = this.formModal.value.diameter;
    if(this.formModal.value.family === "FPP"){
      if (diameter <= 1300) { this.formModal.controls['subfamily'].setValue("FP1") } else
      if (diameter <= 1600) { this.formModal.controls['subfamily'].setValue("FP2") } else
      if (diameter <= 2000) { this.formModal.controls['subfamily'].setValue("FP3") } else
      if (diameter <= 3500) { this.formModal.controls['subfamily'].setValue("FP4") }
      else { this.formModal.controls['subfamily'].setValue("FP5") }
    } else if(this.formModal.value.family === "CPP") {
      if (diameter <= 2600) { this.formModal.controls['subfamily'].setValue("CP1") } else
      if (diameter <= 3400) { this.formModal.controls['subfamily'].setValue("CP2") } else
      if (diameter <= 4600) { this.formModal.controls['subfamily'].setValue("CP3") } else
      if (diameter <= 6600) { this.formModal.controls['subfamily'].setValue("CP4") }
      else { this.formModal.controls['subfamily'].setValue("CP5") }
    }
  }

  setSurfaces(){
    if(this.formModal.value.finish_class === "S"){
      this.formModal.controls['surfaces'].setValue(true);
      this.formModal.controls['surfaces'].disable();
    }
    else{
      this.formModal.controls['surfaces'].setValue(false);
      this.formModal.controls['surfaces'].enable();
    }
  }

  closeModal(data: any) {
    this.activeModal.close(data);
  }

  ngOnInit(): void {
  }

}
