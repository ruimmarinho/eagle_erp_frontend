import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalculatorPageComponent } from './calculator-page/calculator-page.component';
import {RouterModule} from "@angular/router";
import {CalculatorRoutes} from "./calculator.routing";
import {QuotationOrderComponent} from "./components/quotation-order/quotation-order.component";
import {ReactiveFormsModule} from "@angular/forms";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import { EstimatedTimeComponent } from './components/estimated-time/estimated-time.component';
import {NgbDatepickerModule} from "@ng-bootstrap/ng-bootstrap";
import { ProductModalComponent } from './components/product-modal/product-modal.component';



@NgModule({
    declarations: [
        CalculatorPageComponent,
        QuotationOrderComponent,
        EstimatedTimeComponent,
        ProductModalComponent
    ],
    exports: [
        CalculatorPageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(CalculatorRoutes),
        ReactiveFormsModule,
        FontAwesomeModule,
        NgbDatepickerModule,
    ]
})
export class CalculatorModule { }
