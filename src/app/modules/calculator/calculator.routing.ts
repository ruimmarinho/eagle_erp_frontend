import { Routes } from '@angular/router';
import {CalculatorPageComponent} from "./calculator-page/calculator-page.component";


export const CalculatorRoutes: Routes = [
  {
    path: '',
    component: CalculatorPageComponent,
    data: {
      title: 'Calculator',
      urls: [
        { title: 'Calculator', url: '/calculator' }
      ]
    }
  }
];
