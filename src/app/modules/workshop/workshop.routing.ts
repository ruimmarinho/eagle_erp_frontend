import { Routes } from '@angular/router';
import {WorkshopViewComponent} from "./workshop-view/workshop-view.component";

export const WorkshopRoutes: Routes = [
  {
    path: '',
    component: WorkshopViewComponent,
    data: {
      title: 'Workshop View',
      urls: [
        { title: 'Workshop View', url: '/workshopview' },
      ]
    }
  }
];
