import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {RestApiService} from "../../../../core/http/rest-api.service";
import * as moment from "moment";
import {NavigationStart, Router} from "@angular/router";


interface Workers {
  id: number,
  name: string,
  duration: number,
  start: string,
  end: string
}

interface Resume {
  tasks: number
  estimated: number
  worked: number
  pending: number
  in_progress: number
  done: number
}

@Component({
  selector: 'app-workshop-taskslist',
  templateUrl: './workshop-taskslist.component.html',
  styleUrls: ['./workshop-taskslist.component.css']
})
export class WorkshopTaskslistComponent implements OnInit, OnChanges {

  @Input() data: any;
  @Output() tasksCahnge: EventEmitter<any> =   new EventEmitter();

  machine: any;
  workers: any;

  tasks: any
  timerTasks: any;

  resume!: Resume;

  constructor(private rest: RestApiService, private router: Router) { }

  ngOnInit(): void {
    this.router.events.subscribe((event) => {
      if(event instanceof NavigationStart) {
        if (this.timerTasks) {
          clearInterval(this.timerTasks);
        }
      }
    })
    this.rest.getWorkers().subscribe( workers => {
      this.workers = workers
      console.log(this.workers)
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!changes['data'].firstChange) {
      const data = changes['data'].currentValue
      console.log(data)
      if(data.machine){
        this.machine = data.machine.name
        this.getTasks()
        this.timerTasks = setInterval( () => {
          this.getTasks();
        }, 10000)
      }
    }
  }

  getTasks(){
    this.rest.getTasks(this.data.startDate, this.data.endDate, this.data.machine.id).subscribe( (tasks: any) => {
      // console.log(tasks)
      if(!(JSON.stringify(this.tasks)==JSON.stringify(tasks))){
        this.tasks = tasks;
        this.getResume();
      }
    })
  }

  getResume(){
    let total_estimated = 0;
    let total_worked = 0;
    let total_pending = 0;
    let total_in_progress = 0;
    let total_done = 0;

    this.tasks.forEach( (ot: any) => {
      const reality_start = ot.reality.slots ? ot.reality.slots[ot.reality.slots.length-1].start_time ? ot.reality.slots[ot.reality.slots.length-1].start_time : '' : ''
      const reality_end = ot.reality.slots ? ot.reality.slots[ot.reality.slots.length-1].end_time ? ot.reality.slots[ot.reality.slots.length-1].end_time : '' : ''
      const duration_real = moment.duration(moment(reality_end).diff(moment(reality_start)));

      const resume_duration = parseFloat(duration_real.asHours().toFixed(2));

      total_estimated += ot.duration;

      if(resume_duration){
        total_worked += resume_duration;
      }

      if(ot.status === "Pending" || ot.status === "Paused"){
        total_pending += 1;
      }
      else if (ot.status === "InProgress") {
        total_in_progress += 1;
      }
      else if(ot.status === "Finished") {
        total_done += 1;
      }
    })

    this.resume = {
      tasks: this.tasks.length,
      estimated: total_estimated,
      worked: total_worked,
      pending: total_pending,
      in_progress: total_in_progress,
      done: total_done
    }
  }

  getWorkerName(id: number){
    // @ts-ignore
    return this.workers.some( worker => worker.id === id) ? this.workers.find( worker => worker.id === id).name : "Ze";
  }

}
