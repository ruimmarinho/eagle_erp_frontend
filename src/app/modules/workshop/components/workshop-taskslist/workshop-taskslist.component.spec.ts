import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshopTaskslistComponent } from './workshop-taskslist.component';

describe('WorkshopTaskslistComponent', () => {
  let component: WorkshopTaskslistComponent;
  let fixture: ComponentFixture<WorkshopTaskslistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkshopTaskslistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshopTaskslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
