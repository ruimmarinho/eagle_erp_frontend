import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import * as moment from "moment";

interface OT {
  reference: string;
  product: string;
  stage: string;
  task: string;
  duration: number;
  status: string;
  estimated_start: string;
  estimated_end: string;
  real_start: string;
  real_end: string;
  real_duration: string;
  remaining: any;
  resume: string;
  show: boolean;
  workers: Workers[]
}

interface Workers {
  id: number,
  name: string,
  duration: number,
  start: string,
  end: string
}

@Component({
  selector: 'app-machine-task',
  templateUrl: './machine-task.component.html',
  styleUrls: ['./machine-task.component.css']
})
export class MachineTaskComponent implements OnInit, OnDestroy {

  @Input() data: any;

  ot!: OT;

  timerInProgress: any;

  constructor() { }

  ngOnInit(): void {
    const task = this.data
    // console.log(task)
    const reality_start = task.reality.slots ? task.reality.slots[task.reality.slots.length-1].start_time ? task.reality.slots[task.reality.slots.length-1].start_time : '' : ''
    const reality_end = task.reality.slots ? task.reality.slots[task.reality.slots.length-1].end_time ? task.reality.slots[task.reality.slots.length-1].end_time : '' : ''
    const estimated_end = task.reality.slots ? task.reality.slots[task.reality.slots.length-1].end_time ? moment(reality_start).add(task.duration, "hours").format("DD-MM-YYYY HH:mm:ss") : moment(reality_start).add(task.duration, "hours").format("DD-MM-YYYY HH:mm:ss") : ''
    const duration_real = moment.duration(moment(reality_end).diff(moment(reality_start)));
    const reality_duration = ('0' + Math.round(duration_real.asHours()).toString()).slice(-2) + ":" + ('0' + duration_real.minutes()).slice(-2) + ":" + ('0' + duration_real.seconds()).slice(-2);
    const duration_diff = moment.duration(moment(estimated_end, 'DD-MM-YYYY HH:mm:ss').diff(moment(reality_end, 'YYYY-MM-DD HH:mm:ss')));
    let resume_duration = '0';
    if(duration_diff.as('milliseconds') < 0){
      resume_duration = '-' + ('0' + Math.round(duration_diff.asHours()).toString()).slice(-2) + ":" + ('0' + Math.abs(duration_diff.minutes())).slice(-2) + ":" + ('0' + Math.abs(duration_diff.seconds())).slice(-2);
    }
    else {
      resume_duration = ('0' + Math.round(duration_diff.asHours()).toString()).slice(-2) + ":" + ('0' + duration_diff.minutes()).slice(-2) + ":" + ('0' + duration_diff.seconds()).slice(-2);
    }

    this.ot = {
      reference: this.getReference(task.reference, "po"),
      product: this.getReference(task.reference, "product"),
      stage: this.getReference(task.reference, "stage"),
      task: this.getReference(task.reference, "task"),
      duration: task.duration,
      status: task.status,
      estimated_start: moment(task.schedule.slots[0].start_time).format("DD-MM-YYYY"),
      estimated_end: moment(task.schedule.slots[task.schedule.slots.length-1].end_time).format("DD-MM-YYYY"),
      real_start: reality_start,
      real_end: reality_end,
      real_duration: reality_duration,
      remaining: () => {
        const duration = moment.duration(moment.utc(estimated_end, "DD-MM-YYYY HH:mm:ss").diff(moment()));
        return ('0' + Math.floor(duration.asHours()).toString()).slice(-2) + ":" + ('0' + duration.minutes()).slice(-2) + ":" + ('0' + duration.seconds()).slice(-2);
      },
      resume: resume_duration,
      show: false,
      workers: []
    }
    // console.log(this.ot)

    this.timerInProgress = setInterval( () => {
      this.ot.remaining();
    }, 1000)
  }

  getReference(ref: string, level: string){
    switch (level) {
      case "po":
        return ref.split("-").slice(0, -3).join("-")
      case "product":
        return ref.split("-")[(ref.split("-").length) - 3];
      case "stage":
        return ref.split("-")[(ref.split("-").length) - 2];
      case "task":
        return ref.split("-")[(ref.split("-").length) - 1];
    }
    return ''
  }

  ngOnDestroy() {
    if(this.timerInProgress){
      clearInterval(this.timerInProgress);
    }
  }

}
