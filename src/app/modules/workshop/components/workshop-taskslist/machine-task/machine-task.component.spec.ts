import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineTaskComponent } from './machine-task.component';

describe('MachineTaskComponent', () => {
  let component: MachineTaskComponent;
  let fixture: ComponentFixture<MachineTaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MachineTaskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
