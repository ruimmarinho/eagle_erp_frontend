import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshopMachinesComponent } from './workshop-machines.component';

describe('MachinesComponent', () => {
  let component: WorkshopMachinesComponent;
  let fixture: ComponentFixture<WorkshopMachinesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkshopMachinesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshopMachinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
