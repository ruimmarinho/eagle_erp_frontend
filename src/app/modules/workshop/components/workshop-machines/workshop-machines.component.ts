import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { faGear, faCalendarDays, faCaretDown, faCaretUp, faList, faTriangleExclamation } from '@fortawesome/free-solid-svg-icons';
import * as moment from "moment";
import {RestApiService} from "../../../../core/http/rest-api.service";
import {forkJoin, subscribeOn} from "rxjs";
import {NavigationStart, Router} from "@angular/router";

@Component({
  selector: 'app-machines',
  templateUrl: './workshop-machines.component.html',
  styleUrls: ['./workshop-machines.component.css']
})
export class WorkshopMachinesComponent implements OnInit {

  @Output() machineSelected: EventEmitter<any> =   new EventEmitter();

  faGear = faGear;
  faCalendarDays = faCalendarDays;
  faCaretDown = faCaretDown;
  faCaretUp = faCaretUp;
  faList = faList;

  faTriangleExclamation = faTriangleExclamation;

  machines: any = []

  timerMachines: any

  constructor(private rest: RestApiService, private router: Router) { }

  ngOnInit(): void {
    this.router.events.subscribe((event) => {
      if(event instanceof NavigationStart) {
        if (this.timerMachines) {
          clearInterval(this.timerMachines);
        }
      }
    })
    this.rest.getMachines().subscribe(machines => {
      this.machines = machines;
      this.setMachineStatus();
    })
    this.timerMachines = setInterval( () => {
      this.setMachineStatus();
    }, 10000)
  }

  setMachineStatus(){
    this.machines.forEach( (machine: any) => {
      this.rest.getStateMachine(machine.id).subscribe( (nTasks: any) => {
        if(nTasks.length > 0){
          machine["status"] = "working";
        }
        else {
          machine["status"] = "stopped";
        }
      })
    })
    // console.log(this.machines)
  }

  machineToParent(machine: {}){
    this.machineSelected.emit(machine)
  }
}
