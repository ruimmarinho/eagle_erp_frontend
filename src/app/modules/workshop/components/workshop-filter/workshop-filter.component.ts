import {Component, ElementRef, EventEmitter, HostListener, OnInit, Output, ViewChild} from '@angular/core';
import * as moment from "moment";
import { faCalendarDays } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-workshop-filter',
  templateUrl: './workshop-filter.component.html',
  styleUrls: ['./workshop-filter.component.css']
})
export class WorkshopFilterComponent implements OnInit {

  @Output() datesChanged: EventEmitter<any> =   new EventEmitter();

  @HostListener('window:resize', ['$event'])
  onWindowResize() {
    this.getChildren(this.selected.type);
  }

  @ViewChild("filters") private filterType: ElementRef<HTMLElement> | any;
  selected: any = {}

  faCalendarDays = faCalendarDays;

  startDate:any;
  endDate:any;

  start!: string
  end!: string

  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      this.getChildren(0)
    }, 100);
  }

  setStartDate(startDate: any){
    const end = moment(startDate.day + "-" + startDate.month + "-" + startDate.year, "DD-MM-YYYY").add(30,"days");
    this.start = moment(startDate.day + "-" + startDate.month + "-" + startDate.year, "DD-MM-YYYY").format('YYYY-MM-DD');
    this.end = moment(startDate.day + "-" + startDate.month + "-" + startDate.year, "DD-MM-YYYY").add(30,"days").format('YYYY-MM-DD');
    this.endDate = {
      year: parseInt(moment(end).format('YYYY')),
      month: parseInt(moment(end).format('MM')),
      day: parseInt(moment(end).format('DD')),
    };
    this.selected = {}
    this.datesToParent()
  }

  setEndDate(startDate: any){
    this.end = moment(startDate.day + "-" + startDate.month + "-" + startDate.year, "DD-MM-YYYY").format('YYYY-MM-DD');
    this.datesToParent()
  }

  getChildren(n: number) {
    const parentElement = this.filterType.nativeElement;
    const firstChild = parentElement.children[n];
    this.selected = {
      "type": n,
      "width": firstChild.offsetWidth,
      "left": firstChild.offsetLeft
    }
    switch (n){
      case 0:
        this.dateWeek()
        break;
      case 1:
        this.date2Weeks()
        break;
      case 2:
        this.dateMonth()
        break;
    }
    this.datesToParent()
    this.startDate = null;
    this.endDate = null;
  }

  dateWeek(){
    const today = moment();
    this.start = today.startOf('isoWeek').isoWeekday(1).format('YYYY-MM-DD');
    this.end = today.endOf('isoWeek').format('YYYY-MM-DD');
  }
  date2Weeks(){
    const today = moment();
    this.start = today.startOf('isoWeek').isoWeekday(1).format('YYYY-MM-DD');
    this.end = today.add(1, 'weeks').endOf('isoWeek').format('YYYY-MM-DD');
  }
  dateMonth(){
    const today = moment();
    this.start = today.startOf('month').format('YYYY-MM-DD');
    this.end = today.endOf('month').format('YYYY-MM-DD');
  }

  datesToParent(){
    this.datesChanged.emit({startDate: this.start, endDate: this.end})
  }

}
