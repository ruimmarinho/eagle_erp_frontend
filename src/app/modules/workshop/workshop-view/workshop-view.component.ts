import {Component, OnInit, ViewChild} from '@angular/core';
import {WorkshopMachinesComponent} from "../components/workshop-machines/workshop-machines.component";

@Component({
  selector: 'app-workshop-view',
  templateUrl: './workshop-view.component.html',
  styleUrls: ['./workshop-view.component.css']
})
export class WorkshopViewComponent implements OnInit {

  startDate: any;
  endDate: any;
  machine: any;

  constructor() { }

  ngOnInit(): void {}

  datesChangedHandler(data: any) {
    this.startDate = data.startDate
    this.endDate = data.endDate
    console.log(this.startDate)
    console.log(this.endDate)
  }

  machineChangedHandler(data: any) {
    this.machine = data
    console.log(this.machine)
  }

}
