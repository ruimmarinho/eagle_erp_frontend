import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkshopViewComponent } from './workshop-view/workshop-view.component';
import {RouterModule} from "@angular/router";
import {WorkshopRoutes} from "./workshop.routing";
import { WorkshopMachinesComponent } from './components/workshop-machines/workshop-machines.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule} from "@angular/forms";
import {NgbCollapseModule, NgbDatepickerModule} from "@ng-bootstrap/ng-bootstrap";
import { WorkshopFilterComponent } from './components/workshop-filter/workshop-filter.component';
import { WorkshopTaskslistComponent } from './components/workshop-taskslist/workshop-taskslist.component';
import { MachineTaskComponent } from './components/workshop-taskslist/machine-task/machine-task.component';



@NgModule({
  declarations: [
    WorkshopViewComponent,
    WorkshopMachinesComponent,
    WorkshopFilterComponent,
    WorkshopTaskslistComponent,
    MachineTaskComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(WorkshopRoutes),
    FontAwesomeModule,
    FormsModule,
    NgbDatepickerModule,
    NgbCollapseModule,
  ]
})
export class WorkshopModule { }
