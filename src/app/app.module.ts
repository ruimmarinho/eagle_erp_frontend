import { NgModule } from '@angular/core';
import {
  APP_BASE_HREF,
  CommonModule,
  LocationStrategy,
  PathLocationStrategy
} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {FullComponent} from "./layouts/full/full.component";

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {PerfectScrollbarModule} from "ngx-perfect-scrollbar";
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';
import {SpinnerComponent} from "./shared/components/spinner/spinner.component";
import {AppRoutingModule} from "./app-routing.module";
import {BlankComponent} from "./layouts/blank/blank.component";
import {NavigationComponent} from "./core/header/header-navigation/navigation.component";
import {SidebarComponent} from "./core/header/sidebar/sidebar.component";
import {BreadcrumbComponent} from "./core/header/breadcrumb/breadcrumb.component";
import {HttpClientModule} from "@angular/common/http";
import { ModalWarningComponent } from './shared/components/modal-warning/modal-warning.component';
import { ToastNotificationComponent } from './shared/components/toast-notification/toast-notification.component';
import { ToastComponent } from './shared/components/toast-notification/toast/toast.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 2,
  wheelPropagation: true
};

@NgModule({
  declarations: [
    AppComponent,
    SpinnerComponent,
    FullComponent,
    BlankComponent,
    NavigationComponent,
    BreadcrumbComponent,
    SidebarComponent,
    ModalWarningComponent,
    ToastNotificationComponent,
    ToastComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    NgbModule,
    AppRoutingModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/'},
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    {
      provide: LocationStrategy,
      useClass: PathLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
