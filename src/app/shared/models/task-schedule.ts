export class TaskSchedule {
  id!: number;
  task_type!: string;
  task_reference!: string

  slots!: Slots[];
  old_schedules!: OldSchedules[];

  constructor(args: TaskSchedule) {
    Object.assign(this, args);
  }
}

export class OldSchedules {
  creation!: number;
  reason!: string;

  slots!: Slots[];

  constructor(args: OldSchedules) {
    Object.assign(this, args);
  }
}

export class Slots {
  start_time!: number;
  end_time!: number;
  machine!: number;
  worker!: number

  constructor(args: Slots) {
    Object.assign(this, args);
  }
}
