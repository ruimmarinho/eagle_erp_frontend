
export class Machine {
  id!: number;
  name!: string;
  max_diameter!: number;
  max_weight!: number;
  max_height!: number;
  max_length!: number;

  applications!: Applications[]


  constructor(args: Machine) {
    Object.assign(this, args);
  }
}

export class Applications {
  task_type!: string;
  subfamily!: string;
  priority!: number;

  constructor(args: Applications) {
    Object.assign(this, args);
  }
}
