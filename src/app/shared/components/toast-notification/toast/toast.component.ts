import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { faCircleXmark, faCircleCheck, faCircleExclamation, faCircleInfo } from '@fortawesome/free-solid-svg-icons';
import {ToastService} from "../../../../core/services/toast.service";

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.css']
})
export class ToastComponent implements OnInit, OnDestroy {

  @Input() toast!: any;
  @Input() index!: any;

  faCircleXmark = faCircleXmark;
  faCircleCheck = faCircleCheck;
  faCircleExclamation = faCircleExclamation;
  faCircleInfo = faCircleInfo

  timer: any

  constructor(public toastService: ToastService) { }

  ngOnInit(): void {
    this.timer = setInterval(() => {
      this.toast.duration -= 1;
      if(this.toast.duration <= 0){
        this.toastService.removeToast(this.index)
      }
    }, 1000)
  }

  ngOnDestroy() {
    clearInterval(this.timer)
  }

}
