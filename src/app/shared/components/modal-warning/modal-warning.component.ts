import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-modal-warning',
  templateUrl: './modal-warning.component.html',
  styleUrls: ['./modal-warning.component.css']
})
export class ModalWarningComponent implements OnInit {

  @Input() public title: any;
  @Input() public text: any;
  @Input() public btns: any;

  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  btnClick(func: any){
    // @ts-ignore
    this[func]();
  }

  confirm() {
    this.activeModal.close(true);
  }

  close() {
    this.activeModal.close(false);
  }

}
