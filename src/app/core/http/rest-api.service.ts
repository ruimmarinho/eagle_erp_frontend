import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, Observable, retry, throwError} from "rxjs";
import {ToastService} from "../services/toast.service";

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  // apiURL = 'http://127.0.0.1:8000';
  // apiURL = 'http://192.168.31.49:8000';
  // apiURL = 'http://62.28.41.158:8000';
  apiURL = 'http://192.168.1.31:8000';

  constructor(private http: HttpClient, private toastService: ToastService) { }

  //#region Workers
  getWorkers(){
    return this.http.get(this.apiURL + '/workermgmt/workers/',
      {responseType: 'json'}).pipe(retry(1), catchError(this.handleError))
  }
  createWorker(worker: any){
    return this.http.post(this.apiURL + '/workermgmt/workers/', worker,
      {responseType: 'text'}).pipe(retry(1), catchError(this.handleError))
  }
  deleteWorker(wid: number){
    return this.http.delete(this.apiURL + '/workermgmt/workers/' + wid + '/',
      {responseType: 'text'}).pipe(retry(1), catchError(this.handleError))
  }
  //#endregion

  //#region Tasks
  getTasks(start: string, end: string, machine: number){
    return this.http.get(this.apiURL + '/tasks/?from_date=' + start +'&to_date=' + end + '&machine=' + machine,
      {responseType: 'json'}).pipe(retry(1), catchError(this.handleError))
  }
  getStateMachine(machine: number){
    return this.http.get(this.apiURL + '/tasks/?machine=' + machine + "&status=InProgress",
      {responseType: 'json'}).pipe(retry(1), catchError(this.handleError))
  }
  createTask(task: any){
    return this.http.post(this.apiURL + '/taskmgmt/tasks/', task,
      {responseType: 'text'}).pipe(retry(1), catchError(this.handleError))
  }
  deleteTask(tid: number){
    return this.http.delete(this.apiURL + '/taskmgmt/tasks/' + tid + '/',
      {responseType: 'text'}).pipe(retry(1), catchError(this.handleError))
  }
  //#endregion

  //#region Products
  getProducts(){
    return this.http.get(this.apiURL + '/productmgmt/products/',
      {responseType: 'json'}).pipe(retry(1), catchError(this.handleError))
  }
  createProduct(product: any){
    return this.http.post(this.apiURL + '/productmgmt/products/', product,
      {responseType: 'text'}).pipe(retry(1), catchError(this.handleError))
  }
  deleteProduct(pid: number){
    return this.http.delete(this.apiURL + '/productmgmt/products/' + pid + '/',
      {responseType: 'text'}).pipe(retry(1), catchError(this.handleError))
  }
  //#endregion

  //#region Machine
  getMachines(): Observable<any> {
    return this.http.get(this.apiURL + '/machinemgmt/machines/',
      {responseType: 'json'}).pipe(retry(1), catchError(this.handleError))
  }
  createMachine(machine: any){
    return this.http.post(this.apiURL + '/machinemgmt/machines/', machine,
      {responseType: 'text'}).pipe(retry(1), catchError(this.handleError))
  }
  deleteMachine(mid: number){
    return this.http.delete(this.apiURL + '/machinemgmt/machines/' + mid + '/',
      {responseType: 'text'}).pipe(retry(1), catchError(this.handleError))
  }
  //#endregion


  //#region Calculate
  getProductCycles(orderToCalculate: any){
    return this.http.post(this.apiURL + '/quotationmgmt/order/calculate/', orderToCalculate,
      {observe: 'response'}).pipe(retry(1), catchError(this.handleError.bind(this)))
  }
  //#endregion

  //#region Quotation Order
  getQuotationOrder(){
    return this.http.get(this.apiURL + '/quotationmgmt/order/?status=Pending',
      {responseType: 'json'}).pipe(retry(1), catchError(this.handleError))
  }
  addQuotationOrder(qo: any){
    return this.http.post(this.apiURL + '/quotationmgmt/order/', qo,
      {observe: 'response'}).pipe(retry(1), catchError(this.handleError.bind(this)))
  }
  updateQuotationOrder(qo: any){
    return this.http.put(this.apiURL + '/quotationmgmt/order/', qo,
      {responseType: 'json'}).pipe(retry(1), catchError(this.handleError))
  }
  //#endregion

  //#region Purchase Order
  getPurchaseOrder(){
    return this.http.get(this.apiURL + '/purchasemgmt/order/',
      {responseType: 'json'}).pipe(retry(1), catchError(this.handleError))
  }

  getPurchaseOrderByDates(start: any, end: any){
    return this.http.get(this.apiURL + '/purchasemgmt/order/?from_date='+ start +'&to_date=' + end,
      {responseType: 'json'}).pipe(retry(1), catchError(this.handleError))
  }

  addPurchaseOrder(reference: string){
    console.log(this.apiURL + '/purchasemgmt/order/'+ reference +'/')
    return this.http.post(this.apiURL + '/purchasemgmt/order/'+ reference +'/', '',
      {responseType: 'json'}).pipe(retry(1), catchError(this.handleError))
  }
  //#endregion

  //#region Task Schedules
  getTaskSchedules(start: string, end: string){
    return this.http.get(this.apiURL + '/taskmgmt/task_schedule/?from_date=' + start +'&to_date=' + end,
      {responseType: 'json'}).pipe(retry(1), catchError(this.handleError))
  }


  //#endregion

  //#region Working Slots
  getWorkingSlots(){
    return this.http.get(this.apiURL + '/workingslotmgmt/working_slot/',
      {responseType: 'json'}).pipe(retry(1), catchError(this.handleError))
  }
  //#endregion

  // Error handling
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      this.toastService.addToast({type: "error", text: error.statusText, duration: 3})
    }
    // window.alert(errorMessage);
    console.log(errorMessage);
    return throwError(() => {
      return errorMessage;
    });
  }
}
