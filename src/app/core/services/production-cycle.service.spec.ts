import { TestBed } from '@angular/core/testing';

import { ProductionCycleService } from './production-cycle.service';

describe('ProductionCycleService', () => {
  let service: ProductionCycleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductionCycleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
