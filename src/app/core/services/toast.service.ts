import { Injectable } from '@angular/core';

interface toast {
  type: string,
  text: string,
  duration: number,
}

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  toasts: toast[] = []

  constructor() { }

  addToast(toast: any){
    this.toasts.push({
      type: toast.type,
      text: toast.text,
      duration: toast.duration
    })
  }

  removeToast(index: number){
    this.toasts.splice(index,1)
  }

}
