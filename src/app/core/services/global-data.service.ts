import { Injectable } from '@angular/core';
import {Machine} from "../../shared/models/machine";
import {RestApiService} from "../http/rest-api.service";
import {Subject} from "rxjs";
import {TaskSchedule} from "../../shared/models/task-schedule";

@Injectable({
  providedIn: 'root'
})
export class GlobalDataService {

  machines: Machine[] = []
  public machineObservable = new Subject<Machine[]>();

  taskSchedule: TaskSchedule[] = []
  public taskScheduleObservable = new Subject<TaskSchedule[]>();

  constructor(private rest: RestApiService) {}

  getMachines(){
    this.rest.getMachines().subscribe((data: any) => {
      this.machines = data;
      this.machineObservable.next(data);
    })
  }

  getTaskSchedules(){
    this.rest.getMachines().subscribe((data: any) => {
      this.machines = data;
      this.machineObservable.next(data);
    })
  }

}
