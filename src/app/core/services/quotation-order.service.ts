import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QuotationOrderService {

  quotations: any = [
    {
      "id" : 1651223201022,
      "reference" : "QO-27520386",
      "customer" : "PROMARIN",
      "creation" : "2022-04-29T11:06:41.000+0000",
      "target_date" : "2022-04-30T00:00:00.000+0000",
      "products" : [
        {
          "reference" : "P1",
          "family" : "FPP",
          "subfamily" : "FP1",
          "finish_class" : "I",
          "material" : "bronce",
          "diameter" : 2000,
          "weight" : 2000,
          "dar" : 0,
          "z" : 4,
          "surfaces" : false,
          "cone_diameter" : 0,
          "templates" : false,
          "porting_hole" : false,
          "additional_requirements" : [

          ]
        }
      ]
    }
  ]

  constructor() { }
}
