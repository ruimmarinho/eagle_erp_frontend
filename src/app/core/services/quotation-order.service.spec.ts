import { TestBed } from '@angular/core/testing';

import { QuotationOrderService } from './quotation-order.service';

describe('QuotationOrderService', () => {
  let service: QuotationOrderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuotationOrderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
