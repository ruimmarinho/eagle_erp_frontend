import { Injectable } from '@angular/core';
import * as moment from "moment";

@Injectable({
  providedIn: 'root'
})
export class FunctionsService {

  constructor() { }

  splitSlots(tasks: any){
    const slotsTasks: any = []
    tasks.forEach( (task: any) => {
      task.slots.forEach( (slot: any) => {
        const start = this.timestampToMoment(slot.start_time).startOf("day");
        const end = this.timestampToMoment(slot.end_time).startOf("day");
        const daysDiff = end.diff(start, "days")
        if(daysDiff === 0){
          slotsTasks.push({
            product_reference: task.product_reference,
            type: task.task_type,
            start: this.momentToString(this.timestampToMoment((slot.start_time * 1000)), true),
            end: this.momentToString(this.timestampToMoment((slot.end_time * 1000)), true),
            duration:  this.getMomentHours(this.timestampToMoment((slot.start_time * 1000)), this.timestampToMoment((slot.end_time * 1000))),
            color: slot.color
          })
        }
        else{
          for(let i = 0; i < daysDiff; i++){
            if(i === 0){
              slotsTasks.push({
                product_reference: task.product_reference,
                type: task.task_type,
                start: this.momentToString(this.timestampToMoment((slot.start_time * 1000)), true),
                end: this.momentToString(this.timestampToMoment((slot.start_time * 1000)).endOf("day"), true),
                duration:  this.getMomentHours(this.timestampToMoment((slot.start_time * 1000)), this.timestampToMoment((slot.start_time * 1000)).endOf("day")),
                color: slot.color
              })
            }
            if(i !== 0 && i !== (daysDiff-1)){
              slotsTasks.push({
                product_reference: task.product_reference,
                type: task.task_type,
                start: this.momentToString(this.timestampToMoment((slot.start_time * 1000)).add(i,"days").startOf("day"), true),
                end: this.momentToString(this.timestampToMoment((slot.start_time * 1000)).add(i,"days").endOf("day"), true),
                duration:  this.getMomentHours(this.timestampToMoment((slot.start_time * 1000)).add(i,"days").startOf("day"), this.timestampToMoment((slot.start_time * 1000)).add(i,"days").endOf("day")),
                color: slot.color
              })
            }
            if(i === (daysDiff-1)){
              slotsTasks.push({
                product_reference: task.product_reference,
                type: task.task_type,
                start: this.momentToString(this.timestampToMoment((slot.end_time * 1000)).startOf("day"), true),
                end: this.momentToString(this.timestampToMoment((slot.end_time * 1000)), true),
                duration:  this.getMomentHours(this.timestampToMoment((slot.end_time * 1000)).startOf("day"), this.timestampToMoment((slot.end_time * 1000))),
                color: slot.color
              })
            }
          }
        }
      });
    });
    return slotsTasks
  }

  timestampToMoment(timestamp: number){
    return moment.utc(timestamp)
  }

  momentToString(moment: moment.Moment, full: boolean){
    if(full){
      return moment.format("DD-MM-YYYY HH:mm")
    }
    else{
      return moment.format("DD-MM-YYYY")
    }
  }

  getMomentHours(start: moment.Moment, end: moment.Moment){
    // console.log(start)
    // console.log(end)
    // console.log(parseInt(moment.duration(end.diff(start)).asHours().toFixed(1)))
    return parseInt(moment.duration(end.diff(start)).asHours().toFixed(1));
  }
}
