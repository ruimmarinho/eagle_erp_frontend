import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PurchaseOrderService {

  private purchases = new BehaviorSubject(null);
  current_purchases = this.purchases.asObservable();

  constructor() { }

  addPurchase(products: any) {
    this.purchases.next(products)
  }
}
