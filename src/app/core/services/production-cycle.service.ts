import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProductionCycleService {

  private products = new BehaviorSubject(null);
  current_products = this.products.asObservable();

  constructor() { }

  changeProducts(products: any) {
    this.products.next(products)
  }

}
