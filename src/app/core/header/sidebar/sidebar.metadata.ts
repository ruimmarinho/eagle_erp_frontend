// Sidebar route metadata
import {IconProp} from "@fortawesome/fontawesome-svg-core";

export interface RouteInfo {
  path: string;
  title: string;
  icon: IconProp;
  class: string;
  label: string;
  labelClass: string;
  extralink: boolean;
  submenu: RouteInfo[];
}
