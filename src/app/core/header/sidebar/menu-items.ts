import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
    // {
    //     path: '',
    //     title: 'Personal',
    //     icon: '',
    //     class: 'nav-small-cap',
    //     label: '',
    //     labelClass: '',
    //     extralink: true,
    //     submenu: []
    // },
    // {
    //     path: '',
    //     title: 'Calculadora OT',
    //     icon: 'mdi mdi-gauge',
    //     class: 'has-arrow',
    //     label: '4',
    //     labelClass: 'label label-rouded label-themecolor',
    //     extralink: false,
    //     submenu: [
    //         {
    //             path: '/dashboard/dashboard1',
    //             title: 'Modern',
    //             icon: '',
    //             class: '',
    //             label: '',
    //             labelClass: '',
    //             extralink: false,
    //             submenu: []
    //         }
    //     ]
    // },
    {
      path: '/eagleview',
      title: 'Eagle View',
      icon: 'gauge-high',
      class: '',
      label: '',
      labelClass: '',
      extralink: false,
      submenu: []
    },
    // {
    //     path: '/dashboard',
    //     title: 'Overview',
    //     icon: 'gauge-high',
    //     class: '',
    //     label: '',
    //     labelClass: '',
    //     extralink: false,
    //     submenu: []
    // },
    // {
    //   path: '/calculator',
    //   title: 'Calculadora OT',
    //   icon: 'calculator',
    //   class: '',
    //   label: '',
    //   labelClass: '',
    //   extralink: false,
    //   submenu: []
    // },
    // {
    //     path: '/orders',
    //     title: 'Orders',
    //     icon: 'tasks',
    //     class: '',
    //     label: '',
    //     labelClass: '',
    //     extralink: false,
    //     submenu: []
    // },
    // {
    //     path: '/planner',
    //     title: 'Planificación',
    //     icon: 'calendar-days',
    //     class: '',
    //     label: '',
    //     labelClass: '',
    //     extralink: false,
    //     submenu: []
    // },
    // {
    //     path: '/workshopview',
    //     title: 'Vista Taller',
    //     icon: 'industry',
    //     class: '',
    //     label: '',
    //     labelClass: '',
    //     extralink: false,
    //     submenu: []
    // },
    // {
    //     path: '/dashboard4',
    //     title: 'Picaje Efectivo',
    //     icon: 'industry',
    //     class: '',
    //     label: '',
    //     labelClass: '',
    //     extralink: false,
    //     submenu: []
    // },
    // {
    //     path: '/dashboard4',
    //     title: 'Material',
    //     icon: 'bahai',
    //     class: '',
    //     label: '',
    //     labelClass: '',
    //     extralink: false,
    //     submenu: []
    // },
    // {
    //     path: '/management',
    //     title: 'Gestión',
    //     icon: 'list-check',
    //     class: 'has-arrow',
    //     label: '',
    //     labelClass: '',
    //     extralink: false,
    //     submenu: [
    //             {
    //                 path: '/management/workers',
    //                 title: 'Workers',
    //                 icon: 'list',
    //                 class: '',
    //                 label: '',
    //                 labelClass: '',
    //                 extralink: false,
    //                 submenu: []
    //             },
    //             {
    //               path: '/management/tasks',
    //               title: 'Tasks',
    //               icon: 'list',
    //               class: '',
    //               label: '',
    //               labelClass: '',
    //               extralink: false,
    //               submenu: []
    //             },
    //             {
    //               path: '/management/products',
    //               title: 'Products',
    //               icon: 'list',
    //               class: '',
    //               label: '',
    //               labelClass: '',
    //               extralink: false,
    //               submenu: []
    //             },
    //             {
    //               path: '/management/machines',
    //               title: 'Machines',
    //               icon: 'list',
    //               class: '',
    //               label: '',
    //               labelClass: '',
    //               extralink: false,
    //               submenu: []
    //             }
    //         ]
    // },
    // {
    //     path: '/dashboard5',
    //     title: 'Config',
    //     icon: 'gear',
    //     class: '',
    //     label: '',
    //     labelClass: '',
    //     extralink: false,
    //     submenu: []
    // }
];
