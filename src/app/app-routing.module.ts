import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { FullComponent } from './layouts/full/full.component';
import {BlankComponent} from "./layouts/blank/blank.component";


export const routes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
      { path: '', redirectTo: '/eagleview', pathMatch: 'full' },
      {
        path: 'eagleview',
        loadChildren: () => import('./modules/eagle-view/eagle-view.module').then(m => m.EagleViewModule)
      },
      // {
      //   path: 'dashboard',
      //   loadChildren: () => import('./modules/dashboards/dashboard.module').then(m => m.DashboardModule)
      // },
      // {
      //   path: 'calculator',
      //   loadChildren: () => import('./modules/calculator/calculator.module').then(m => m.CalculatorModule)
      // },
      // {
      //   path: 'orders',
      //   loadChildren: () => import('./modules/orders/orders.module').then(m => m.OrdersModule)
      // },
      // {
      //   path: 'planner',
      //   loadChildren: () => import('./modules/planner/planner.module').then(m => m.PlannerModule)
      // },
      // {
      //   path: 'workshopview',
      //   loadChildren: () => import('./modules/workshop/workshop.module').then(m => m.WorkshopModule)
      // },
      // {
      //   path: 'management',
      //   loadChildren: () => import('./modules/managements/management.module').then(m => m.ManagementModule)
      // },




      // {
      //   path: 'component',
        // loadChildren: () => import('./component/component.module').then(m => m.ComponentsModule)
      // },
      // { path: 'icons', loadChildren: () => import('./icons/icons.module').then(m => m.IconsModule) },
      // { path: 'forms', loadChildren: () => import('./form/forms.module').then(m => m.FormModule) },
      // { path: 'tables', loadChildren: () => import('./table/tables.module').then(m => m.TablesModule) },
      // { path: 'charts', loadChildren: () => import('./charts/charts.module').then(m => m.ChartModule) },
      // {
      //   path: 'widgets',
      //   loadChildren: () => import('./widgets/widgets.module').then(m => m.WidgetsModule)
      // },
      // {
      //   path: 'extra-component',
      //   loadChildren:
      //     () => import('./extra-component/extra-component.module').then(m => m.ExtraComponentModule)
      // },
      // // { path: 'apps', loadChildren: () => import('./apps/apps.module').then(m => m.AppsModule) },
      // {
      //   path: 'apps/email',
      //   loadChildren: () => import('./apps/email/mail.module').then(m => m.MailModule)
      // },
      // {
      //   path: 'sample-pages',
      //   loadChildren: () => import('./sample-pages/sample-pages.module').then(m => m.SamplePagesModule)
      // }
    ]
  },
  {
    path: '',
    component: BlankComponent,
    children: [
      {
        path: 'authentication',
        loadChildren:
          () => import('./modules/authentication/authentication.module').then(m => m.AuthenticationModule)
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), NgbModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
